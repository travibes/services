<?php
require_once 'classes/config.php';
   $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
   if( mysqli_connect_error()) echo "Failed to connect to MySQL: " . mysqli_connect_error(); 
?>   
<?php

$query = "SELECT serviceProvider.id,product_servicer.serviceCharge,serviceProvider.deliveryCharge,serviceProvider.companyName 
FROM area INNER JOIN area_servicer 
INNER JOIN product 
INNER JOIN product_servicer
INNER JOIN serviceProvider
WHERE (
area.id = area_servicer.areaId
AND product.id = product_servicer.productId
AND area_servicer.serviceProviderId = product_servicer.serviceProviderId
AND product_servicer.serviceProviderId = serviceProvider.id
AND product.productName ='". urldecode($_GET['product']) . 
"' AND area.area_name ='" . urldecode($_GET['area']) .
"' )";
   
// execute query
    $result = mysqli_query($con,$query);
    if(mysqli_num_rows($result) > 0){
      echo "<div style='background-color:#FFFFFF;width:400px;margin:auto;height:350px;overflow:auto'>";
      echo "<div style='width:330px;max-height:300px;margin:auto;margin-top:20px'>";
      echo "<h4 style='color:#666666'>List of service providers</h4>";
        echo "<div  class='list-group'>";
        while($row = mysqli_fetch_assoc($result)) {

            $query1 = "SELECT DISTINCT product.productName
            FROM product
            INNER JOIN product_servicer
            INNER JOIN area
            INNER JOIN area_servicer
            WHERE (
            product_servicer.serviceProviderId ='" . $row['id'] .
            "'AND area_servicer.serviceProviderId ='" . $row['id'] .
            "'AND product.id = product_servicer.productId
            AND area.id = area_servicer.areaId
            AND area_name = '" . urldecode($_GET['area']) .
            "')";

            $result1 = mysqli_query($con,$query1);
            
           echo "<a href='#' class='list-group-item' data-toggle='modal' data-target='#" .$row['id'] ."'>";
           echo "<p style='padding-left:10px;font-size:14px'>Service Charge &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp<span class='badge'>"; echo $row['serviceCharge']; echo "</span></p>";
           echo "<p style='padding-left:10px;font-size:15px'>other services : <small>";
           $product_array = array();
           $counter=0;
           if(mysqli_num_rows($result1) > 0)
           {
            // while(($row1 = mysqli_fetch_assoc($result1)) && ($counter<=3))
            while($row1 = mysqli_fetch_assoc($result1))
                  {
                    $product_array[$counter] = $row1['productName'];
                    $counter++;
                   // echo $row1['productName'];
                  }
                  $counter1 = 0;
                if($counter > 3)
                {
                  foreach ($product_array as $value) {
                    if($counter1<2)
                    {
                        echo $value.", ";
                    }
                    elseif ($counter1==2) {
                        echo $value." and more";
                    }
                     $counter1++;
                    }
                }
                elseif($counter <=3)
                {
                    foreach ($product_array as $value) {
                    if($counter1<$counter-1)
                    {
                        echo $value.", ";
                    }
                    elseif ($counter1==$counter-1) {
                        echo $value;
                    }
                     $counter1++;
                    }
                }
           }
           echo "</small></p>";
           echo "</a>";
        
            // echo "</a>";
            echo "<div class='modal fade' id=" . $row['id'] . " role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>";
                echo "<div class='modal-dialog modal-lg'>";
                    echo "<div class='modal-content'>";
                        echo "<div class='modal-body'>";
                        echo "<div class='row'>";
                           echo "<div class='col-md-4'>";
                               echo "<p style='padding-left:15px;margin-top:10px;font-size:16px'>Other services provided by this service provider</p>";
                               echo "<p style='padding-left:15px;font-size:15px'><small>";
                               if(mysqli_num_rows($result1) > 0)
                               {
                                 $counter1 = 0;
                                 if($counter > 7)
                                 {
                                    foreach ($product_array as $value) {
                                        if($counter1<6)
                                        {
                                           echo $value.", ";
                                        }
                                        elseif ($counter1==7) {
                                           echo $value." and more";
                                        }
                                          $counter1++;
                                    }
                                 }
                                 elseif($counter <=7)
                                 {
                                   foreach ($product_array as $value) {
                                      if($counter1<$counter-1)
                                      {
                                         echo $value.", ";
                                      }
                                      elseif ($counter1==$counter-1) {
                                        echo $value;
                                      }
                                      $counter1++;
                                    }
                                }
                                }
                                echo "</small></p>";
                           echo "</div>";
                           echo "<div class='col-md-4' style='text-align:center'>";
                           echo "<p style='margin-top:10px;font-size:15px'>service charge<br>Rs ",$row['serviceCharge'],"</p>";
                           echo "<p style='margin-top:30px;font-size:15px'>delivery charge<br>Rs ",$row['deliveryCharge'],"</p>";
                           echo "<p style='margin-top:70px'><a href='placeOrder.php?serviceProId=",$row['id'],"&productName=",$_GET['product'],"&serviceCharge=",$row['serviceCharge'],"' class='btn btn-success'>continue</a></p>";
                           echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo "<div>";
                                echo "<p style='margin-left:0px;margin-top:10px;font-size:15px'>company name<br>",$row['companyName'],"</p>";
                                echo "<p style='margin-left:0px;font-size:13px'>note : the service charges 
                                shown here are only the fixed charges taken by the service providers for servicing you.
                                the final price of your service depend on factors such as integration of new parts 
                                or stuffs in exchange of old one.</p>";
                                echo "</div>";
                            echo "</div>";
                        echo "</div>";
                        echo "</div>";
                        echo "<br>";
                    echo "</div>";
                echo "</div>";
            echo "</div>";
        }
        echo "</div>";
        echo "</table></div></div></div>";
    }
    else
    {
      echo "<div style='text-align:center'>there is no such service provider is available for your match.</div>";
    }
?>
<script>
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.document.location = $(this).data("target");
        });
    });
</script>
<!-- 
 -->
        <!-- <a class="navbar-brand hover-mouse" data-toggle="modal" data-target="#myModal">LogIn</a> -->
        <!-- echo "<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title center" id="myModalLabel">Log In</h3>
                    </div>
                    <div class="modal-body">
                        
                    </div>
                    <br>
                </div>
            </div>
        </div> "; -->