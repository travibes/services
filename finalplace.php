<?php
   require_once 'classes/config.php';
   $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
   if( mysqli_connect_error()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
   session_start();
?>

<!-- header section -->

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewpoint" content="width=device-width, initial-scale=1.0">
		<title>Best Services in World</title>

		<!-- CSS files -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.min.css">
		<!-- js files -->
		<script src="js/jquery1.11.2.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="jquery-ui/jquery-ui.min.js"></script>
		</head>
<body>
<!-- header -->
<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 5px;background-color:#ffffff">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
	      	<a class="navbar-brand" href="index.php"><img style="width:90px;" src="images/Drawing.png"></a>
	    </div>
		
	</div>
</nav>	

<!-- storing order details into table -->
<?php

// accessing delivery charges of each service provider
	$delivery_query = "SELECT deliveryCharge,email FROM serviceProvider WHERE id = '" . $_POST['serviceProId'] . "';";
	$result_delivery = mysqli_query($con, $delivery_query);
	$row_delivery = mysqli_fetch_assoc($result_delivery);
	$delivery_charge=$row_delivery['deliveryCharge'];
	$servicer_email = $row_delivery['email'];
	//echo $delivery_charge;

// encrypted string
    $encrypt_service = '';
	for($i=0;$i<12;$i++)
	{
		$encrypt_service .= mt_rand(0,9);
	}
    //echo $encrypt_service;

    // declaring orderstatus
    $orderStatus = "processing";
if(isset($_SESSION['email']))
{

	// accessing user's id
	$user_query = "SELECT id,address FROM customers WHERE email = '" . $_SESSION['email'] . "';";
	$result_id = mysqli_query($con, $user_query);
	$row_id = mysqli_fetch_assoc($result_id);
	$user_id = $row_id['id'];
	$user_address = $row_id['address'];
	//echo $user_address;
    
	// query for inserting address if it creating order first time or it's address is null
    if(is_null($user_address))
    {
       $address_query = "UPDATE customers SET address = '" . $_POST['address'] . "' 
       WHERE email = '" . $_SESSION['email'] ."';";
       mysqli_query($con, $address_query);
    }

	// assigning serviceNumber for regstered orders
	$serviceNumber = "SN" . $encrypt_service;
     //echo $serviceNumber;
	// query to insert order details

	$order_query = "INSERT INTO orders (serviceNumber, customerId, serviceProviderId, 
		email, mobile, name, orderDate, orderTime, serviceDate, serviceCharge, address, 
		productName, deliveryCharge, service_status) VALUES('$serviceNumber', $user_id, ".$_POST['serviceProId'].
		", '".$_SESSION['email']."', '".$_POST['mobile']."', '".$_POST['name'].
		"', DATE_FORMAT(CURDATE(),'%d %b %Y'), CURTIME(), '".$_POST['date']."', '".$_POST['serviceCharge'].
		"', '".$_POST['address']."', '".$_POST['productName']."', $delivery_charge, '$orderStatus');";
   // echo $order_query;
    mysqli_query($con, $order_query);

    // sending email to user after placing service 

     date_default_timezone_set('Etc/UTC');
require 'PHPMailer-master/PHPMailerAutoload.php';
//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
//$mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
// $mail->Host = 'smtp.gmail.com';
// use
 $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "radserving@gmail.com";
//Password to use for SMTP authentication
$mail->Password = "radserve";
//Set who the message is to be sent from
$mail->setFrom('radserving@gmail.com', 'Radserving.com');
//Set an alternative reply-to address
$mail->addReplyTo('reply.radserving@gmail.com', 'Radserving Support');
//Set who the message is to be sent to
$mail->addAddress($_SESSION['email'], 'customer');
//Set the subject line
$mail->Subject = 'Your service is confirmed';
// accessing the password of user
//$password_query = "SELECT password FROM customers WHERE email = '" . $_POST['email'] . "';";
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->Body = "<p>your service for product ".$_POST['productName']." has been placed successfully. Your service number for the requested service is ".$serviceNumber.". We will contact you shortly for further proceedings.</p>";
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//send the message, check for errors
if (!$mail->send()) {
echo "Mailer Error: " . $mail->ErrorInfo;
} 

// sending email to service provider after placing service
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
//$mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
// $mail->Host = 'smtp.gmail.com';
// use
 $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "radserving@gmail.com";
//Password to use for SMTP authentication
$mail->Password = "radserve";
//Set who the message is to be sent from
$mail->setFrom('radserving@gmail.com', 'Radserving.com');
//Set an alternative reply-to address
$mail->addReplyTo('reply.radserving@gmail.com', 'Radserving Support');
//Set who the message is to be sent to
$mail->addAddress($servicer_email, 'customer');
//Set the subject line
$mail->Subject = 'Your service is confirmed';
// accessing the password of user
//$password_query = "SELECT password FROM customers WHERE email = '" . $_POST['email'] . "';";
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->Body = "<p>You have recieved a service request of product ".$_POST['productName']." with service number ".$serviceNumber.". The product which is being serviced will be delivered to you shortly. <br> After the product is delivered to you, now it is your responsibility to take care of that and prevent from any damage till it is back to it's owner.</p>";
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//send the message, check for errors
if (!$mail->send()) {
echo "Mailer Error: " . $mail->ErrorInfo;
} 

?>

<!-- second service done div for registered user -->

<div class="panel panel-default" style="width:70%;margin:auto;margin-top:60px;margin-bottom:150px">
  <div class="panel-heading"><p class="text-muted">service number - <?php echo $serviceNumber; ?></p></div>
  <div class="panel-body" style="height:200px">
    <p class="text-muted" style="font-size:17px">CONGRATULATIONS, Your service has been placed successfully. Your service number is <?php echo $serviceNumber;  ?></p>
    <p class="text-muted" style="font-size:17px">One of our representative will contact you shortly.</p>
    <div style="width:20%;margin:auto;margin-top:6%;">
    <a href="index.php" class="btn btn-success" role="button">Continue servicing</a>
    </div>
  </div>
</div>

<?php
}
else
{

    // assigning serviceNumber for unregstered orders
	$serviceNumber = "USN" . $encrypt_service;
    //echo $serviceNumber;
    // query to insert order details
   
    $order_query = "INSERT INTO nonreg_user (serviceNumber, serviceProviderId, 
		email, mobile, name, orderDate, orderTime, serviceDate, serviceCharge, address, 
		productName, deliveryCharge) VALUES('$serviceNumber', ".$_POST['serviceProId'].", '".$_POST['email']."', '".$_POST['mobile']."', '".$_POST['name']."', CURDATE(), CURTIME(), '".$_POST['date']."', '".$_POST['serviceCharge']."', '".$_POST['address']."', '".$_POST['productName']."', $delivery_charge);";
    //echo $order_query;
    mysqli_query($con, $order_query);

    // sending email to user after placing service 
date_default_timezone_set('Etc/UTC');
require 'PHPMailer-master/PHPMailerAutoload.php';
//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
//$mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
// $mail->Host = 'smtp.gmail.com';
// use
 $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "radserving@gmail.com";
//Password to use for SMTP authentication
$mail->Password = "radserve";
//Set who the message is to be sent from
$mail->setFrom('radserving@gmail.com', 'Radserving.com');
//Set an alternative reply-to address
$mail->addReplyTo('reply.radserving@gmail.com', 'Radserving Support');
//Set who the message is to be sent to
$mail->addAddress($_POST['email'], 'customer');
//Set the subject line
$mail->Subject = 'PHPMailer GMail SMTP test';
// accessing the password of user
//$password_query = "SELECT password FROM customers WHERE email = '" . $_POST['email'] . "';";
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->Body = "<p>your service for product ".$_POST['productName']." has been placed successfully. Your service number for the requested service is ".$serviceNumber.". We will contact you shortly for further proceedings.</p>";
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//send the message, check for errors
if (!$mail->send()) {
echo "Mailer Error: " . $mail->ErrorInfo;
} 

// sending email to service provider after placing service 
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
//$mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
// $mail->Host = 'smtp.gmail.com';
// use
 $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "radserving@gmail.com";
//Password to use for SMTP authentication
$mail->Password = "radserve";
//Set who the message is to be sent from
$mail->setFrom('radserving@gmail.com', 'Radserving.com');
//Set an alternative reply-to address
$mail->addReplyTo('reply.radserving@gmail.com', 'Radserving Support');
//Set who the message is to be sent to
$mail->addAddress($servicer_email, 'customer');
//Set the subject line
$mail->Subject = 'PHPMailer GMail SMTP test';
// accessing the password of user
//$password_query = "SELECT password FROM customers WHERE email = '" . $_POST['email'] . "';";
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->Body = "<p>You have recieved a service request of product ".$_POST['productName']." with service number ".$serviceNumber.". The product which is being serviced will be delivered to you shortly. <br> After the product is delivered to you, now it is your responsibility to take care of that and prevent from any damage till it is back to it's owner.</p>";
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//send the message, check for errors
if (!$mail->send()) {
echo "Mailer Error: " . $mail->ErrorInfo;
} 

?>
<!-- second service done div for unregistered user -->

<div class="panel panel-default" style="width:70%;margin:auto;margin-top:60px;margin-bottom:150px">
  <div class="panel-heading"><p class="text-muted">service number - <?php echo $serviceNumber; ?></p></div>
  <div class="panel-body" style="height:200px">
    <p class="text-muted" style="font-size:17px">CONGRATULATIONS, Your service has been placed successfully. Your service number is <?php echo $serviceNumber;  ?></p>
    <p class="text-muted" style="font-size:17px">One of our representative will contact you shortly.</p>
    <div style="width:20%;margin:auto;margin-top:6%;">
    <a href="index.php" class="btn btn-success" role="button">Continue servicing</a>
    </div>
  </div>
</div>

<?php
}
require_once("footer.php");
?>


</body>
</html>