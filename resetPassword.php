<?php
    require_once 'classes/config.php';
    $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
    if(mysqli_connect_error()) 
        echo "Failed to connect to MySQL: " . mysqli_connect_error(); 
    session_start();
?>   
<?php
    $salt = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
    $saltedPW = $_GET['value'] . $salt;
    $hashedPW = hash('sha256', $saltedPW);

    $query = "UPDATE customers ";
    $query .= "SET password='$hashedPW',salt='$salt' ";
    $query .= "WHERE email='" .$_SESSION['email']. "'";
    // echo "Your password has been reset.";
    //echo $query;
    $result = mysqli_query($con, $query);
    if (!$result) {
        echo "error occured during reset password";
    }
    else
    {
        echo "Your password has been reset.";
    }

?>