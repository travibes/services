<?php
   require_once 'classes/config.php';
   $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
   if( mysqli_connect_error()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Best Services in World</title>

		<!-- CSS files -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">

		<!-- js files -->
		<script src="js/jquery1.11.2.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="jquery-ui/jquery-ui.min.js"></script>

       <!-- save address and mobile -->

      <script>

// ajax function for login
            function ulogin()
          {

            var email = document.getElementById("placeLog_email").value;
            var password = document.getElementById("placeLog_password").value;
            
              if (window.XMLHttpRequest){
               // code for IE7+, Firefox, Chrome, Opera, Safari
               xmlhttp=new XMLHttpRequest();
           }
           else{
           // code for IE6, IE5
               xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
           }
           xmlhttp.onreadystatechange=function(){
               if (xmlhttp.readyState==4 && xmlhttp.status==200){
                var result = xmlhttp.responseText.split("\n");
                var bool = true;
                var a=document.getElementById("place_ulogin");
                for (var i = result.length - 1; i >= 0; i--) {
                  if(result[i] == "successfull")
                  { 
                    bool = false;
                    location.reload();
                  }
                }
                if(bool)
                  a.innerHTML = "your email or password is incorrect, please try again" ;
             }
           }
           xmlhttp.open("POST","user_login.php",true);
           xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
           xmlhttp.send("email="+email+"&password="+password);
          } 


          // ajax function for registration
            function uregistration()
          {

            var email = document.getElementById("placeRegister_email").value;
            var mobile = document.getElementById("placeRegister_mobile").value;
            var password = document.getElementById("placeRegister_password").value;
            
              if (window.XMLHttpRequest){
               // code for IE7+, Firefox, Chrome, Opera, Safari
               xmlhttp=new XMLHttpRequest();
           }
           else{
           // code for IE6, IE5
               xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
           }
           xmlhttp.onreadystatechange=function(){
               if (xmlhttp.readyState==4 && xmlhttp.status==200){
                var result = xmlhttp.responseText.split("\n");
                var bool = true;
                var a=document.getElementById("place_uregister");
                for (var i = result.length - 1; i >= 0; i--) {
                  if(result[i] == "successfull")
                  { 
                    bool = false;
                    location.reload();
                  }
                }
                if(bool)
                  a.innerHTML = "your email is already registered with us, try with another email" ;
             }
           }
           xmlhttp.open("POST","user_register.php",true);
           xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
           xmlhttp.send("email="+email+"&mobile="+mobile+"&password="+password);
          } 

</script>


    </head>
    <body>

    <!-- Header Section -->
		
	<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0px;background-color:#ffffff">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
	      	<a class="navbar-brand" href="index.php"><img style="width:90px;" src="images/Drawing.png"></a>
	    </div>
		
	</div>
</nav>	
	<!-- End Header Section -->

	<div class="jumbotron place" style="margin-top:0px;margin-bottom:0px;overflow:hidden;min-height:650px">
	<div class="row container">
	<div class="col-md-5" style="margin-top:30px">

      <?php
      if(isset($_SESSION['email']))
      {
      	$user_email = $_SESSION['email'];
        $saltQuery2 = "SELECT address, mobile FROM customers WHERE email = '$user_email';";
        $result_pass2 = mysqli_query($con, $saltQuery2);
        $row2 = mysqli_fetch_assoc($result_pass2);
        $area_address = $row2['address'];
        $mobile = $row2['mobile'];
      ?> 
      <p style="text-align:center;padding-top:150px;color:#dddddd">Great Experience That Will Never Feel</p>
      <p style="text-align:center;color:#dddddd">Fast and Trusted Services</p>
        
     <?php
      }
      else
      {
     
         // second
     ?>

         <div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true' style="width:350px;margin:auto">
  <div class='panel panel-default'>
    <div class='panel-heading' role='tab' id='headingOne'>
     <h4 class='panel-title'>
        <a data-toggle='collapse' data-parent='#accordion' href='#collapseOne' aria-expanded='true' aria-controls='collapseOne'>
          Sign up
        </a>
      </h4>
    </div>
    <div id='collapseOne' class='panel-collapse collapse in' role='tabpanel' aria-labelledby='headingOne'>
      <div class='panel-body'>
      <!-- showing error message -->
      <div><p class="text-danger" id="place_uregister" style="font-size:12px"></p></div>
       <!--  signup  -->
      <form>
								<div class='form-group'>
								<input type='text' class='form-control' id='placeRegister_email'placeholder='Email-Id'>
								</div>
								<div class='form-group'>
								<input type='text' class='form-control' id='placeRegister_mobile' placeholder='Mobile'>
								</div>
								<div class='form-group'>
								<input type='password' class='form-control' id='placeRegister_password' placeholder='Enter Password'>
								</div>
								<button type='button' class='btn btn-block btn-info' onclick="uregistration()">signup</button>
	  </form>

      </div>
    </div>
  </div>
  <div class='panel panel-default'>
    <div class='panel-heading' role='tab' id='headingTwo'>
      <h4 class='panel-title'>
        <a class='collapsed' data-toggle='collapse' data-parent='#accordion' href='#collapseTwo' aria-expanded='false' aria-controls='collapseTwo'>
          Login
        </a>
      </h4>
    </div>
    <div id='collapseTwo' class='panel-collapse collapse' role='tabpanel' aria-labelledby='headingTwo'>
      <div class='panel-body'>
          <!-- showing error message -->
      <div><p class="text-danger" id="place_ulogin" style="font-size:12px"></p></div>
       <!--  login -->
       <form>
								<div class='form-group'>
								<input type='text' class='form-control' id='placeLog_email' placeholder='Email-Id'>
								</div>
								<div class='form-group'>
								<input type='password' class='form-control' id='placeLog_password' placeholder='Enter Password'>
								</div>
								<button type='button' class='btn btn-block btn-info' onclick="ulogin()">Login</button>
		 </form>

      </div>
    </div>
  </div>
</div>

        <?php
        }
        ?>

	</div>

    <div class="col-md-2">

    <?php
    if(!isset($_SESSION['email']))
    {
    ?>

    <div style='height:150px;width:0px;margin-top:15px;border-left: solid #dedede;margin:auto'>
    </div>
    <p style='text-align:center;margin-top:10px;color:#b0b0b0'>OR</p>
    <div style='height:150px;width:0px;margin-top:0px;border-left: solid #dedede;margin:auto'>
    </div>

    <?php
    }
    ?>

    </div>

	<div class="col-md-5">

      <?php
      if(isset($_SESSION['email']))
      {

        $saltQuery = "SELECT mobile,address FROM customers WHERE email = '$user_email';";
        $result_pass = mysqli_query($con, $saltQuery);
        $row = mysqli_fetch_assoc($result_pass);
        $mobile = $row['mobile'];
        $address = $row['address'];
        // for address
        if(is_null($address))
        {
        	$placeholder_add = "Enter your address";
        	$value ="";
        }
        else
        {
        	$placeholder_add = "";
        	$value = $address;
        }


         echo "<p style='margin-top:10px;font-size:22px;margin-left:30px;color:#cccccc'>When we will arrive at your house</p>";
         echo "<div style='height:360px;margin-top:10px;width:400px;margin-left:30px'>";
      	// echo "<div style='width:0px;margin:auto;padding-top:0px'>";
      	 echo "<form action='finalplace.php' method='post'>";
									echo "<div class='form-group'>";
									echo "<input type='text' class='form-control' id='name' name='name' placeholder='Enter your name'>";
									echo "</div>";
									echo "<div class='form-group'>";
									echo "<input type='text' class='form-control' id='email' name='email' placeholder='$user_email' disabled>";
									echo "</div>";
									echo "<div class='form-group'>";
									echo "<input type='text' class='form-control' id='mobile' name='mobile' value='$mobile'>";
									echo "</div>";
                  echo "<div class='form-group'>";
                  echo "<input type='text' class='form-control' id='date' name='date' placeholder='Enter service date'>";
                  echo "</div>";
									echo "<div class='form-group'>";
									echo "<input type='text' class='form-control' id='address' name='address' placeholder='$placeholder_add' value='$value'>";
									echo "</div>";
									// hidden values
        ?>
                  <input type='hidden' name='serviceProId' value="<?php echo $_GET['serviceProId']; ?>">
                  <input type='hidden' name='productName' value="<?php echo $_GET['productName']; ?>">
                  <input type='hidden' name='serviceCharge' value="<?php echo $_GET['serviceCharge']; ?>">

				<?php
									echo "<button type='submit' class='btn btn-block btn-success'>Place Service";
									echo "</button>";
		echo "</form>";
		//echo "</div>";
      	echo "</div>";

      }
      else
      {
      	echo "<p style='margin-top:10px;font-size:22px;margin-left:30px;color:#cccccc'>Fill your details and place order as a guest";
      	echo "</p>";
      	echo "<div style='height:360px;margin-top:10px;width:400px;margin-left:30px'>";
      //	echo "<div style='width:370px;margin:auto;padding-top:40px'>";
      	echo "<form action='finalplace.php' method='post'>";
									echo "<div class='form-group'>";
									echo "<input type='text' class='form-control' id='name' name='name' placeholder='Enter your name'>";
									echo "</div>";
									echo "<div class='form-group'>";
									echo "<input type='text' class='form-control' id='email' name='email' placeholder='Enter email'>";
									echo "</div>";
									echo "<div class='form-group'>";
									echo "<input type='text' class='form-control' id='mobile' name='mobile' placeholder='Enter mobile'>";
									echo "</div>";
                  echo "<div class='form-group'>";
                  echo "<input type='text' class='form-control' id='date' name='date' placeholder='Enter service date'>";
                  echo "</div>";
									echo "<div class='form-group'>";
									echo "<input type='text' class='form-control' id='address' name='address' placeholder='Enter your address'>";
									echo "</div>";
                  // hidden values
        ?>
									<input type='hidden' name='serviceProId' value="<?php echo $_GET['serviceProId']; ?>">
									<input type='hidden' name='productName' value="<?php echo $_GET['productName']; ?>">
                  <input type='hidden' name='serviceCharge' value="<?php echo $_GET['serviceCharge']; ?>">

								<?php
									echo "<button type='submit' class='btn btn-block btn-success'>Place Service";
									echo "</button>";
		echo "</form>";
	//	echo "</div>";
      	echo "</div>";
      }

      ?>

	</div>
	</div>
	</div>

  <?php
    require_once("footer.php");
   ?>


  <script>
        $('#date').datepicker({
          })
    </script>

    </body>
    </html>