<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-body">
					     <div class="row" id="show">
					         <div class="col-md-7">
						          <div style="margin-top:0px;padding-top:0px;">
							      <ul class="nav nav-pills" style="padding-left:80px">
								  <li role="presentation" class="active"><a href="#login" aria-controls="login" role="tab" data-toggle="tab">login</a></li>
								  <li role="presentation"><a href="#signup" aria-controls="signup" role="tab" data-toggle="tab">signup</a></li>
							      </ul>
						          <div class="tab-content" style="margin-top:20px;">
						          <!-- login -->
					                   <div role="tabpanel" class="tab-pane active" id="login">
					    		           <div style="padding-right:80px;padding-left:80px;border-right:0px #eee solid;float:left">
					    		    	   <!-- showing error message -->
						                   <div><p class="text-danger" id="ulogin" style="font-size:12px"></p></div>
					    		              <form id="log">
										      <div class="form-group">
											  <input data-validation="required" type="text" class="form-control" id="log_email" placeholder="Email-Id">
										      </div>
										      <div class="form-group">
											  <input data-validation="required" type="password" class="form-control" id="log_password" placeholder="Enter Password">
										      </div>
										      <button type="button" class="btn  btn-block  btn-info" onclick="ulogin()">Login</button>
									          </form>
									          <button type="button" class="btn btn-link" id="forgotPass" style="float:right">forgot password?</button>
									          <br><br>
									          <p class="text-muted" style="text-align:left;padding-bottom:50px"><small>
									        	        By logging in, you agree to our terms & condition and that you have carefully read our privacy policy
									          </small></p>

									        </div>
			                            </div>

			                            <div role="tabpanel" class="tab-pane" id="signup">
			                            <!-- sign up  -->
			    		                    <div style="padding-right:80px;padding-left:80px;border-right:0px #eee solid;float:left">
			    		                      <!-- showing error message -->
						                   <div><p class="text-danger" id="uregister" style="font-size:12px"></p></div>
			    		                        <form id="sign">
								                <div class="form-group">
								                <input data-validation="required" type="text" class="form-control" id="reg_email" placeholder="Email-Id">
								                </div>
								                <div class="form-group">
								                <input data-validation="required" type="text" class="form-control" id="reg_mobile" placeholder="Mobile">
								                </div>
								                <div class="form-group">
								                <input data-validation="required" type="password" class="form-control" id="reg_password" placeholder="Enter Password">
								                </div>
								                <button type="button" class="btn btn-block btn-info" onclick="uregistration()">signup</button>
							                    </form>
							                    <br>
							                    <p class="text-muted" style="text-align:left;padding-bottom:50px"><small>
							                    By signing up, you agree to our terms & condition and that you have carefully read our privacy policy
							                    </small></p>

							                </div>
			                            </div>
			                        </div>
		<!-- last -->
		    			
					                </div>
					            </div>
					        <div class="col-md-5">
					          <div style="margin-top:80px">
					        	 <h4 style="text-align:center">Why Do Signup</h4>
					        	 <p class="text-muted" style="text-align:center;margin-top:30px">No need to fill details again and again</p>
					        	 <p class="text-muted" style="text-align:center">You can see your service orders history</p>
					        	 <p class="text-muted" style="text-align:center">You will get updates on latest services</p>
					          </div>
					        </div>
					       </div>
					       <!-- replacable forgot div -->

                            <div class='row' id='forgotModal' style='height:342px'>
        	                    <div class='col-md-6'>
        	                    <p style='margin-left:80px;font-size:20px;margin-top:80px'>Forgot Password<p>
        	                    <!-- error messege -->
        	                    <div><p class="text-danger" id="uforgot" style="font-size:12px;margin-left:80px"></p></div>
        	                      <div style='width:350px;margin-left:80px;margin-top:20px;position:relative'>
        	                      <form>
        	                      <div class='form-group'>
        	                      <input type='text' class='form-control' id='forgot_email' placeholder='Email-Id'>
        	                      </div>
        	                      <button type='button' class='btn btn-info' onclick="forgotpass()">submit</button>
        	                      </form>
        	                      </div>
        	                    </div>
        	                    <div class='col-md-6'>
        	                      <button type='button' class='btn btn-info' id='forgotBack' style='margin-left:300px'>back</button>
        	                      <div style='width:270px;height:125px;margin:auto;background-color:#eee;margin-top:50px'>
        	                      <p style='color:#777;font-size:13px;padding-left:10px;padding-top:20px;padding-right:10px'>
        	                      After submitting your registered email, we will send you the password associated with your account.
        	                      if you want to change your password, then please go to your profile.
        	                      </p>
        	                      </div>
        	                    </div>
        	                </div>

                           <!-- end of forgot div -->
					</div>
					
			</div>
	</div>
</div>

