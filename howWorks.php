<div class="container" style="margin-top: 60px">
    <p style="text-align:center;font-size:35px;">How It Works</p>
    <div class="row" style="margin-top:50px;">
        <div class="col-md-4 text-center">
            <canvas id="myCanvas1" style="height:70px;">
                Your browser
            </canvas>
            <p style="font-size:18px">Search for a required service</p>
        </div>
        <div class="col-md-4 text-center">
            <canvas id="myCanvas2" style="height:70px;">
                Your browser
            </canvas>
            <p style="font-size:18px">Choose among listed service providers and place the service</p>
        </div>
        <div class="col-md-4 text-center">
            <canvas id="myCanvas3" style="height:70px;">
                Your browser
            </canvas>
            <p style="font-size:18px">Get your service at the doorstep and pay after work</p>
        </div>
    </div>
</div>