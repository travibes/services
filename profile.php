<?php
   require_once 'classes/config.php';
   $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
   if( mysqli_connect_error()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewpoint" content="width=device-width, initial-scale=1.0">
		<title>Best Services in World</title>

		<!-- CSS files -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
		<!-- js files -->
		<script src="js/jquery1.11.2.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

		 <!-- modify address and mobile -->
		<script>
		function modifyAddress()
		{
			document.getElementById('textAddress').disabled=false;
			return false;
		}

    function modifyMobile()
    {
      document.getElementById('mobile').disabled=false;
      return false;
        }

		</script>

   <!-- save address and mobile -->

      <script>
function saveAddress(modifiedValue)
 { 
 	alert(modifiedValue);
 	var xmlhttp;
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  
  xmlhttp.onreadystatechange=function(){
    if (xmlhttp.readyState==4 && xmlhttp.status==200){   
    	  
    	document.getElementById('textAddress').disabled = true;
      document.getElementById('resetAddressResponse').innerHTML = xmlhttp.responseText;
      $("#resetAddressResponse").fadeOut(3000); 
  	}
}

  xmlhttp.open("GET","saveAddress.php?value="+modifiedValue,true);
  xmlhttp.send();

  
}

function saveMobile(modifiedValue)
 { 
  alert(modifiedValue);
  var xmlhttp;
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  
  xmlhttp.onreadystatechange=function(){
    if (xmlhttp.readyState==4 && xmlhttp.status==200){   
        
      document.getElementById('mobile').disabled = true; 
      document.getElementById('resetMobileResponse').innerHTML = xmlhttp.responseText;
      $("#resetMobileResponse").fadeOut(3000);
    }
}

  xmlhttp.open("GET","saveMobile.php?value="+modifiedValue,true);
  xmlhttp.send();

  
}

function savePassword(modifiedValue)
 { 
  alert(modifiedValue);
  var xmlhttp;
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  
  xmlhttp.onreadystatechange=function(){
    if (xmlhttp.readyState==4 && xmlhttp.status==200){   
        
      document.getElementById('rePassword').value = '';
      document.getElementById('resetResponse').innerHTML = xmlhttp.responseText;
      $("#resetResponse").fadeOut(3000);
    }
}

  xmlhttp.open("GET","resetPassword.php?value="+modifiedValue,true);
  xmlhttp.send();

  
}
</script>

	</head>
    <body>
        <!-- header -->
<nav class="navbar navbar-default" role="navigation" style="margin-bottom:5px;background-color:#ffffff">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
	      	<a class="navbar-brand" href="index.php"><img style="width:90px;" src="images/Drawing.png"></a>
	    </div>
		
	</div>
</nav>	

<!-- vertical tabs -->
<div style="width:400px;margin:auto;margin-top:60px;margin-bottom:220px">
      <?php
      if(isset($_SESSION['email']))
      {
      	$user_email = $_SESSION['email'];
        $saltQuery2 = "SELECT address, mobile FROM customers WHERE email = '$user_email';";
        $result_pass2 = mysqli_query($con, $saltQuery2);
        $row2 = mysqli_fetch_assoc($result_pass2);
        $area_address = $row2['address'];
        $mobile = $row2['mobile'];
      ?> 

  <!-- Nav tabs -->
  <ul class="nav nav-pills nav-stacked" role="tablist">
    <li role="presentation" class="active"><a href="#resetPassword" aria-controls="resetPassword" role="tab" data-toggle="tab">Change password</a></li>
    <li role="presentation"><a href="#resetAddress" aria-controls="resetAddress" role="tab" data-toggle="tab">Change Address</a></li>
    <li role="presentation"><a href="#resetMobile" aria-controls="resetMobile" role="tab" data-toggle="tab">Change Mobile</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="resetPassword">
    	<form id="resPassword">
    	<div class="form-group" style="margin-top:40px">
    	 <input type="text" class="form-control" id="rePassword" onchange="savePassword(this.value)" placeholder="Enter new password">
	     <button type='button' class='btn btn-info btn-sm' style='margin-left:170px;margin-top:12px'>reset</button>
    	</div>
    	</form>
    	<p class="bg-success" id="resetResponse"></p>
    </div>
    <div role="tabpanel" class="tab-pane" id="resetAddress">
    	 <div style='width:400px;margin:auto;margin-top:40px'>
	        <textarea class='form-control' id='textAddress' rows='4' onchange="saveAddress(this.value)" disabled><?php echo $area_address; ?> </textarea>
	        <button type='button' class='btn btn-info btn-sm' style='margin-left:115px;margin-top:12px' onclick='modifyAddress();'>modify address</button>
	        <button type='button' class='btn btn-info btn-sm' style='margin-left:10px;margin-top:12px'>save</button>
        </div>
        <p class="bg-success" id="resetAddressResponse"></p>
    </div>
    <div role="tabpanel" class="tab-pane" id="resetMobile">
    	<div class='form-group' style='margin-top:40px;margin-left:auto;margin-right:auto;width:400px;'>
	        <div class='input-group'>
	        	<span class='input-group-addon'>mobile</span>
	        	<input type='text' class='form-control' id='mobile' onchange="saveMobile(this.value)" aria-describedby='mobile' value='<?php echo $mobile ?>' disabled>
	        </div>
	        <button type='button' class='btn btn-info btn-sm' style='margin-left:140px;margin-top:12px' onclick='modifyMobile();'>modify</button>
	        <button type='button' class='btn btn-info btn-sm' style='margin-left:10px;margin-top:12px'>save</button>
      	</div>
        <p class="bg-success" id="resetMobileResponse"></p>
    </div>
  </div>
<?php
}
else
{
?>
 <div style="margin-bottom:350px">Sorry, you are not a registered user. please login to see your profile.</div>
<?php
}
?> 
</div>

<?php
    require_once("footer.php");
   ?>

    </body>
</html>