<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0px;background-color:#ffffff">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
	      	<a class="navbar-brand" href="index.php"><img style="width:120px;margin-top:-10px" src="images/logo.jpg"></a>
	    </div>
		
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav navbar-right">
		<?php
		if(!isset($_SESSION['email']))
	    {
		?>
		<li><a href="serviceProvider_reg.php" target="_blank">Become service provider</a></li>
		<!-- <a href="fair.php" class="btn btn-link navbar-left" role="button">Become service provider</a> -->
		<?php
	    }
		?>
		<!-- <a class="btn btn-link"  id="loghide" data-toggle="modal" data-target="#myModal">LogIn</a> -->
		<li><a href="#" id="loghide" data-toggle="modal" data-target="#myModal">Login</a></li>
		</ul>
		</div>
	</div>
</nav>	