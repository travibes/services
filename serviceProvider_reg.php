<?php
   require_once 'classes/config.php';
   $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
   if( mysqli_connect_error()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewpoint" content="width=device-width, initial-scale=1.0">
		<title>Best Services in World</title>
    <meta name="description" content="Register for free on Radserving and start selling home services online." />
    <meta name="Keywords" content="service provider on Radserving, register on Radserving, service provider registration, sell service, electrical repairing, sanitary works, plumber, furniture repairing, tailor service" />

		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap-multiselect.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">

        
        <script src="js/jquery1.11.2.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-multiselect.js"></script>

		    <script type="text/javascript">

         $(document).ready(function() {
        $("#multiselect2").multiselect({
          buttonWidth : '320px',
          nonSelectedText : 'select the products you want to serviced',
          maxHeight : 140
        });
        });

         function populateProduct(category_name)
         {
              if (window.XMLHttpRequest){
              // code for IE7+, Firefox, Chrome, Opera, Safari
              xmlhttp=new XMLHttpRequest();
          }
          else{// code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange=function(){
              if (xmlhttp.readyState==4 && xmlhttp.status==200){
                  document.getElementById("multiselect2").innerHTML=xmlhttp.responseText;
                  $("#multiselect2").multiselect('rebuild');
//$("#multiselect2").multiselect();
            }
          }
          xmlhttp.open("GET","populateCategoryProduct.php?category_name="+category_name,true);
          xmlhttp.send();
         }
        </script>
		
		
	</head>
	<body>

<!-- Header Section -->
		<nav class="navbar navbar-default" role="navigation" style="margin-bottom:5px;background-color:#ffffff">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
			      	<a class="navbar-brand" href="index.php"><img style="width:90px;" src="images/Drawing.png"></a>
			    </div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<p class="navbar-brand" style="color:#f0ad4e">Service Provider Section</p>
        <p class="navbar-text navbar-right" style="font-weight:bold;font-size:18px;color:#777777">Contact us- <a href="#" class="navbar-link" style="color:#555555">7042 785 251</a></p>

				</div>
			</div>	
		</nav>

<!-- jumbotron -->

<div class="container">
<div class="jumbotron servicer">
<div class="row">
<div class="col-md-8">
<p style="padding-top:150px;padding-left:40px;color:#eeeeee;font-size:25px;font-weight:bold">We are here to help you increase your demand</p>
<p style="padding-left:40px;color:#eeeeee;font-size:25px;font-weight:bold">of services online.</p>
</div>
<div class="col-md-4" style="backgroun-color:#eeeeee;">
<h4 style="color:#f0ad4e">REGISTER NOW & start selling services online</h4>
			    				<form method="post" action="serviceProvider_finalreg.php">
									<div class="form-group">
										<input type="text" class="form-control" id="company" name="company" placeholder="company name">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" id="email" name="email" placeholder="email">
									</div>
									<div class="form-group">
										<input type="text"  class="form-control" id="mobile" name="mobile" placeholder="mobile">
									</div>
                                     <div class="form-group">
										<input type="text" class="form-control" id="city" name="city" placeholder="city">
									</div>
									
									<div class="form-group">
										<select id="multiselect1" multiple="multiple" name="select1[]">
										  <?php
                     $query_area = "SELECT area_name FROM area;";
                                         $result_area = mysqli_query($con, $query_area);
                    while($row_area = mysqli_fetch_assoc($result_area))
                    {
                      echo "<option>"; echo $row_area['area_name']; echo"</option>";
                    }
                    ?>
										</select>
									</div>
									<div class="form-group">
										<select class="form-control" id="selectCategory" name="productCategory" onchange="populateProduct(this.value)">
                    <option selected disabled>Please select</option>
										<?php
										 $query = "Select categoryName from category;";
                                         $result = mysqli_query($con, $query);
										while($row = mysqli_fetch_assoc($result))
										{
											echo "<option>"; echo $row['categoryName']; echo"</option>";
										}
										?>
										</select>
									</div>
                                 
									<div class="form-group">
										<select id="multiselect2" multiple="multiple" name="select2[]">
										</select>
									</div>

								   
									<div class="form-group">
										<input type="text" class="form-control" id="address" name="address" placeholder="enter your address">
									</div>
								
										<button type="submit" class="btn btn-block btn-info" name="serviceProviderForm">Submit</button>
									</form>
</div>
</div>
</div>
</div>

<!-- canvas -->
       <div class="container" style="margin-top: 15px">
       <p style="text-align:center;font-size:35px;">How To List Yourself As a Service Provider</p>
       <div class="row" style="margin-top:70px;">
       <div class="col-md-4 text-center">
           <canvas id="myCanvas1" style="height:70px">
Your browser</canvas>
       <p style="font-size:18px">Register Yourself</p>
       </div>
        <div class="col-md-4 text-center">
                 <canvas id="myCanvas2" style="height:70px;">
Your browser</canvas>
        <p style="font-size:18px">Our representative will contact you shortly</p>
        </div>
          <div class="col-md-4 text-center">
                 <canvas id="myCanvas3" style="height:70px;">
Your browser</canvas>
        <p style="font-size:18px">Start Selling Online</p>
        </div>
       </div>
       </div>

       <!-- we are here section -->

       <div style ='background-color: #5d6b69;height:390px;margin-top:80px'>
       <div class="container">
      <p style="font-size:25px;margin-top:50px;text-align:center">We Are Here To Help You Successfully</p>
      <p style="font-size:25px;text-align:center">Increase Your Service Demand</p>
      <div class="row" style="margin-top:40px">
      <div class="col-md-4" style="color:#bbbbbb">
      <p>After successfully recieving your details</p>
      <p>we analyze tha data you have given to us</p>
      <p>and according to that we list you in our </p>
      <p>online service world.</p>
      </div>
      <div class="col-md-4" style="color:#bbbbbb">
      <p>Once you are listed in our website you can</p>
      <p>sell your services online right at that moment.</p>
      <p>our representative will be in contact with you</p>
      <p>continously after registration for any kind of</p>
      <p>help.</p>
      </div>
      <div class="col-md-4" style="color:#bbbbbb">
      <p>Listing of services on radserving is absolutely</p>
      <p>free. We does not charge anything to you for</p>
      <p>listing your services online.</p>
      </div>
      </div>
      </div>
       </div>

    <!-- header -->

    <div style="height:50px">
    <p style="text-align:center;margin-top:60px";>For any query mail us at : radserving@gmail.com</p>
    </div>


        <script>
        $(document).ready(function() {
        $("#multiselect1").multiselect({
        	enableFiltering : true,
        	buttonWidth : '320px',
        	nonSelectedText : 'select different areas you want to serviced',
        	maxHeight : 180
        });
        });
		</script>


        <script>
       
		</script>

       <!-- canvas script -->
       <script>

       var c = document.getElementById("myCanvas1");
       var ctx = c.getContext("2d");
       ctx.fillStyle = "#f0ad4e";
       ctx.beginPath();
       ctx.arc(144, 75, 65, 0, 2 * Math.PI);
       ctx.closePath();
       ctx.fill();

       ctx.font = 60 +"px serif";
       ctx.fillStyle = 'white';
       ctx.textAlign = 'center';
       ctx.fillText('1', 144, 90);



       var c = document.getElementById("myCanvas2");
       var ctx = c.getContext("2d");
       ctx.fillStyle = "#f0ad4e";
       ctx.beginPath();
       ctx.arc(144, 75, 65, 0, 2 * Math.PI);
       ctx.closePath();
       ctx.fill();

       ctx.font = 60 +"px serif";
       ctx.fillStyle = 'white';
       ctx.textAlign = 'center';
       ctx.fillText('2', 144, 90);

       var c = document.getElementById("myCanvas3");
       var ctx = c.getContext("2d");
       ctx.fillStyle = "#f0ad4e";
       ctx.beginPath();
       ctx.arc(144, 75, 65, 0, 2 * Math.PI);
       ctx.closePath();
       ctx.fill();

       ctx.font = 60 +"px serif";
       ctx.fillStyle = 'white';
       ctx.textAlign = 'center';
       ctx.fillText('3', 144, 90);

</script>


	</body>
	</html>