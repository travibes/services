<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewpoint" content="width=device-width, initial-scale=1.0">
		<title>Best Services in World</title>

		<!-- CSS files -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
       
		<!-- js files -->
		<script src="js/jquery1.11.2.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

		</head>
<body>
<!-- header -->
<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 5px;background-color:#ffffff">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
	      	<a class="navbar-brand" href="index.php"><img style="width:90px;" src="images/Drawing.png"></a>
	    </div>
		
	</div>
</nav>	

<div style="text-align:center;margin-top:80px;margin-bottom:200px">
 <h1>What We Do At radserving</h1>
 <p style="font-size:17px;padding-top:20px;color:#777777">
 We are the fastest growing online service marketplace dedicated to provide home services
 <br>
 in electronics, sanitary, furniture and tailoring domain.In electronics category we are providing
 <br>the repairing services for generally all electronic products. In sanitary category we are providing
 <br> all the major plumbing services at your doorstep. In furniture category we are providing repairing 
 <br>
 services in generally all furniture products (sofa,table,bed e.t.c).In tailoring category we are
 <br>
 providing tailoring services for both men and women at the doorstep.
 </p>
</div>


<?php
 		require_once("footer.php");
?>

</body>
</html>