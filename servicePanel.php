
<div class="cs" style="margin-top:0px;padding-top:0px;">
			<!-- tabs -->
			<ul class="nav nav-pills" style="margin-left:6%;padding-top:10px">
				<li class="active servicetabs"><a href="#electronic" aria-controls="electronic" role="tab" data-toggle="tab" id="el">Electronics</a></li>
				<li><a href="#sanitary" aria-controls="sanitary" role="tab" data-toggle="tab">Sanitary</a></li>
				<li><a href="#furniture" aria-controls="furniture" role="tab" data-toggle="tab">furniture</a></li>
				<li><a href="#tailor" aria-controls="tailor" role="tab" data-toggle="tab">tailor</a></li>
			</ul>
		
			
			<!-- Tab Panes -->
		<div class="no-padding" style="height:100vh">	
			<div class="tab-content" style="margin-top:20px;">
					<!-- Electronics -->
			    <div role="tabpanel" class="tab-pane active" id="electronic">
			    		<div class="container">
			    			<div class="md-col-4" style="float:left">
			    				<h1 style="color:#cccccc">Choose Electronics Service</h1>
			    				<!-- all the fiels are not filled completely error -->
			    				<div id="serviceError1"></div>
			    				<form id="electronics_form" name="electronics_form">
									 <div class="form-group">
										<!-- <input type="text" class="form-control" id="city" placeholder="Enter City"> -->
										<select data-validation="required" class="form-control" id="city">
                                            <option selected disabled>Select City</option> 
                                            <option value="Delhi Ncr">Delhi & Ncr</option>
                                        </select>
									</div> 
									<div class="form-group">
                                     <select data-validation="required" id="area_dropdown">
										<?php
										 $query = "Select area_name from area;";
                                         $result = mysqli_query($con, $query);
										while($row = mysqli_fetch_assoc($result))
										{
											echo "<option value=". urlencode($row['area_name']) .">"; echo $row['area_name']; echo"</option>";
										}
										?>
										</select>
									</div>
									<div class="form-group">
										<select data-validation="required" class="form-control" id="product_dropdown">
										<option selected disabled>Select Product</option>
										<?php
										 $query = "SELECT productName FROM product WHERE categoryId='1';";
                                         $result = mysqli_query($con, $query);
										while($row = mysqli_fetch_assoc($result))
										{
											echo "<option value=". urlencode($row['productName']) .">"; echo $row['productName']; echo"</option>";
										}
										?>
										</select>
									</div>
									<input type="hidden" id="elec_hiddencatId" value="1">
								
										<button type="button" class="btn btn-block btn-info" onclick="showfunction(city.value,area_dropdown.value,product_dropdown.value,elec_hiddencatId.value);">Submit</button>
								</form>
			    			</div>
			    			<div class="md-col-8" style="float:left;">
			    				<div id="serviceProviderList1" style="margin:auto;margin-top:52px">
			    				</div>
			    			</div>
			    			
			    		</div>
			    		
			    </div>
			    <!-- sanitary -->
			    <div role="tabpanel" class="tab-pane" id="sanitary">
			    <div class="container">
			    			<div class="md-col-4" style="float:left">
			    				<h1 style="color:#cccccc">Choose sanitary Service</h1>
			    				<!-- all the fiels are not filled completely error -->
			    				<div id="serviceError2"></div>
			    				<form id="sanitary_form" name="sanitary_form">
									<div class="form-group">
										<!-- <input type="text" class="form-control" id="san_city" placeholder="Enter City"> -->
										<select data-validation="required" class="form-control" id="san_city">
                                            <option selected disabled>Select City</option> 
                                            <option value="Delhi Ncr">Delhi & Ncr</option>
                                        </select>
									</div>
									<div class="form-group">
										<select data-validation="required" id="san_area_dropdown">
										<?php
										 $query = "Select area_name from area;";
                                         $result = mysqli_query($con, $query);
										while($row = mysqli_fetch_assoc($result))
										{
											echo "<option value=". urlencode($row['area_name']) .">"; echo $row['area_name']; echo"</option>";
										}
										?>
										</select>
									</div>
									<div class="form-group">
										<select data-validation="required" class="form-control" id="san_product_dropdown">
										<option selected disabled>Select Product</option>
										<?php
										 $query = "SELECT productName FROM product WHERE categoryId='2';";
                                         $result = mysqli_query($con, $query);
										while($row = mysqli_fetch_assoc($result))
										{
											echo "<option value=". urlencode($row['productName']) .">"; echo $row['productName']; echo"</option>";
										}
										?>
										</select>
									</div>
									<input type="hidden" id="san_hiddencatId" value="2">
								
										<button type="button" class="btn btn-block btn-info" onclick="showfunction(san_city.value,san_area_dropdown.value,san_product_dropdown.value,san_hiddencatId.value);">Submit</button>
									</form>
			    			</div>
			    			<div class="md-col-8" style="float:left;">
			    				<div id="serviceProviderList2" style="margin:auto;margin-top:52px">
			    				</div>
			    			</div>
			    			
			    		</div>
			    </div>
			    <!-- furniture -->
			    <div role="tabpanel" class="tab-pane" id="furniture">
			    	  <div class="container">
			    			<div class="md-col-4" style="float:left">
			    				<h1 style="color:#cccccc">Choose Furniture Service</h1>
			    				<!-- all the fiels are not filled completely error -->
			    				<div id="serviceError3"></div>
			    				<form id="furniture_form" name="furniture_form">
									<div class="form-group">
										<!-- <input type="text" class="form-control" id="fur_city" placeholder="Enter City"> -->
										<select data-validation="required" class="form-control" id="fur_city">
                                            <option selected disabled>Select City</option> 
                                            <option value="Delhi Ncr">Delhi & Ncr</option>
                                        </select>
									</div>
									<div class="form-group">
										 <select data-validation="required" id="fur_area_dropdown">
										<?php
										 $query = "Select area_name from area;";
                                         $result = mysqli_query($con, $query);
										while($row = mysqli_fetch_assoc($result))
										{
											echo "<option value=". urlencode($row['area_name']) .">"; echo $row['area_name']; echo"</option>";
										}
										?>
										</select>
									</div>
									<div class="form-group">
										<select data-validation="required" class="form-control" id="fur_product_dropdown">
										<option selected disabled>Select Product</option>
										<?php
										 $query = "SELECT productName FROM product WHERE categoryId='3';";
                                         $result = mysqli_query($con, $query);
										while($row = mysqli_fetch_assoc($result))
										{
											echo "<option value=". urlencode($row['productName']) .">"; echo $row['productName']; echo"</option>";
										}
										?>
										</select>
									</div>
									<input type="hidden" id="fur_hiddencatId" value="3">
								
										<button type="button" class="btn btn-block btn-info" onclick="showfunction(fur_city.value,fur_area_dropdown.value,fur_product_dropdown.value,fur_hiddencatId.value);">Submit</button>
									</form>
			    			</div>
			    			<div class="md-col-8" style="float:left;">
			    				<div id="serviceProviderList3" style="margin:auto;margin-top:52px">
			    				</div>
			    			</div>
			    			
			    		</div>
			    </div>
			    <!-- tailor -->
			    <div role="tabpanel" class="tab-pane" id="tailor">
			    	<div class="container">
			    			<div class="md-col-4" style="float:left">
			    				<h1 style="color:#cccccc">Choose Tailoring Service</h1>
			    				<!-- all the fiels are not filled completely error -->
			    				<div id="serviceError4"></div>
			    				<form id="tailor_form" name="tailor_form">
									<div class="form-group">
										<!-- <input type="text" class="form-control" id="tail_city" placeholder="Enter City"> -->
										<select data-validation="required" class="form-control" id="tail_city">
                                            <option selected disabled>Select City</option> 
                                            <option value="Delhi Ncr">Delhi & Ncr</option>
                                        </select>
									</div>
									<div class="form-group">
										 <select data-validation="required" id="tail_area_dropdown">
										<?php
										 $query = "Select area_name from area;";
                                         $result = mysqli_query($con, $query);
										while($row = mysqli_fetch_assoc($result))
										{
											echo "<option value=". urlencode($row['area_name']) .">"; echo $row['area_name']; echo"</option>";
										}
										?>
										</select>
									</div>
									<div class="form-group">
										<select data-validation="required" class="form-control" id="tail_product_dropdown">
										<option selected disabled>Select Product</option>
										<?php
										 $query = "SELECT productName FROM product WHERE categoryId='4';";
                                         $result = mysqli_query($con, $query);
										while($row = mysqli_fetch_assoc($result))
										{
											echo "<option value=". urlencode($row['productName']) .">"; echo $row['productName']; echo"</option>";
										}
										?>
										</select>
									</div>
									<input type="hidden" id="tail_hiddencatId" value="4">
								
										<button type="button" class="btn btn-block btn-info" onclick="showfunction(tail_city.value,tail_area_dropdown.value,tail_product_dropdown.value,tail_hiddencatId.value);">Submit</button>
									</form>
			    			</div>
			    			<div class="md-col-8" style="float:left;">
			    				<div id="serviceProviderList4" style="margin:auto;margin-top:52px">
			    				</div>
			    			</div>
			    			
			    		</div>
			    </div>
			</div>
		</div>
</div>