<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewpoint" content="width=device-width, initial-scale=1.0">
		<title>Best Services in World</title>

		<!-- CSS files -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
       
		<!-- js files -->
		<script src="js/jquery1.11.2.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

		</head>
<body>
<!-- header -->
<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 5px;background-color:#ffffff">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
	      	<a class="navbar-brand" href="index.php"><img style="width:90px;" src="images/Drawing.png"></a>
	    </div>
		
	</div>
</nav>	

<div style="margin-bottom:60px">
<h3 style="text-align:center;margin-top:20px">Radserving</h3>
<div style="width:900px;margin:auto">
<h4 style="margin-top:40px">Terms & Conditions</h4>
<p>
Your use of the Service constitutes your acceptance of and agreement to all of the terms and conditions 
in these Terms of Use and the Privacy Policy and your representation that you are adult enough to use 
the services as per laws of the land.
</p>
<p>
Your use of the Service after such posting will constitute your acceptance of and agreement to such changes. 
Therefore, you should frequently review these Terms of Use and the Privacy Policy to see if there has been 
any update and changes incorporated. 
</p>
<p>
This Terms of Use is subject to change at any time, effective upon posting on the Service. Radserving.com is 
an online service marketplace for house hold services that enables the connection between individuals seeking to 
obtain services (“Radserving Customer”) and/or individuals seeking to provide services (“Radserving Service 
Provider”). Radserving Customer and Radserving Service Provider together are hereinafter referred to as “Users” 
and individually as “User”. Those certain services requested by the Radserving Customer, which are to be 
completed by the Radserving Service Provider, are hereinafter referred to as “Services”.
</p>
<h4 style="margin-top:30px">Collection of your Personal Information</h4>
<p>
Radserving.com has limited control over the quality, timing or legality of Services delivered by Radserving 
Service Provider. Radserving.com makes only a sincere attempt to provide best service and hence does not take 
full responsibility of suitability, reliability, timeliness, or accuracy of the Services requested and 
provided by Users. Although Radserving.com does perform background checks of Radserving Service Provider, Radserving.com 
cannot confirm that each User is who they claim to be. Radserving.com does not assume any responsibility for 
the accuracy or reliability of this information or any information on the Service.
</p>
<p>
NEITHER ZIMMBER.COM NOR ITS AFFILIATES OR LICENSORS IS RESPONSIBLE FOR THE CONDUCT, WHETHER ONLINE OR 
OFFLINE, OF ANY USER OF THE SERVICE. ZIMMBER.COM AND ITS AFFILIATES AND LICENSORS WILL NOT BE LIABLE FOR 
ANY CLAIM, INJURY OR DAMAGE ARISING IN CONNECTION WITH YOUR USE OF THE SERVICE.
</p>
<h4 style="margin-top:30px">Transaction</h4>
<p>
Radserving Customer offer Service Payments for each requested service. Three days after a service is 
scheduled, if there is no complaint by the Radserving Customer, the service will be marked as closed by 
Radserving, the agreed upon payment will be transferred to the Radserving.com’s account. The Service Payment must 
be paid through the Zimmber.com web services to the extent possible. Any Service Payments paid in cash 
outside of the Zimmber.com Web Service are NOT subject to refunds. 
</p>
<h4 style="margin-top:30px">Release</h4>
<p>
Radserving.com expressly disclaims any liability beyond service contract value that may arise between Users 
of its Service
</p>
<p>
Radserving.com is only an aggregator of service providers. Though we take utmost care in collecting information 
and background check of service providers still we may have missed out any information unknowingly and/or 
Users have not have shared the correct and all information with Radserving.com. Hence if any dispute arises 
with one or more Users, you release Radserving.com (and our officers, directors, agents, investors, 
subsidiaries, and employees) from any and all claims, demands, or damages (actual or consequential) of 
every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out 
of or in any way connected with such disputes.
</p>
<h4 style="margin-top:30px">Links to Other Web Sites</h4>
<p>
Links (such as hyperlinks) from Radserving.com to other sites on the Web do not constitute as the endorsement 
by Radserving.com of those sites or their content. Such links are provided as an information service, for 
reference and convenience only. Radserving.com does not control any such sites and is not responsible for 
their content. The existence of links on the Service to such websites (including without limitation 
external websites that are framed by the Radserving.com Service as well as any advertisements displayed 
in connection therewith) does not mean that Radserving.com endorses any of the material on such websites, 
or has any association with their operators. It is your responsibility to evaluate the content and 
usefulness of the information obtained from other sites.
The use of any website controlled, owned or operated by third parties is governed by the terms and 
conditions of use and privacy policies for those websites, and not by Radserving.com's Terms of Use or 
Privacy Policy. You access such third-party websites at your own risk. Radserving.com expressly disclaims 
any liability arising in connection with your use and/or viewing of any websites or other material 
associated with links that may appear on the Service. You hereby agree to hold Radserving.com harmless 
from any liability that may result from the use of links that may appear on the Service.
</p>
<h4 style="margin-top:30px">Public Areas</h4>
<p>
The Service may contain profiles, email systems, blogs, message boards, applications, job postings, 
chat areas, news groups, forums, communities and/or other message or communication facilities 
(“Public Areas”) that allow Users to communicate with other Users. You may only use such community areas 
to send and receive messages and material that are relevant and proper to the applicable forum. Without 
limitation, you may not:
<ul style="list-style-type:square">
<li>
Upload files that contain viruses, Trojan horses, corrupted files, or any other similar software that may 
damage the operation of another's computer.
</li>
<li>
Defame, abuse, harass, stalk, threaten or otherwise violate the legal rights (such as, but not limited to, 
rights of privacy and publicity) of others.
</li>
<li>
Use the Service for any purpose which is in violation of local, state, national, or international law.
</li>
<li>
Publish, post, upload, distribute or disseminate any profane, defamatory, infringing, obscene or unlawful 
topic, name, material or information.
</li>
<li>
Upload files that contain software or other material that violates the intellectual property rights (or rights of 
privacy or publicity) of any third party.
</li>
<li>
Advertise or offer to sell any goods or services for any commercial purpose on the Service which are not 
relevant to the services offered on the Service.
</li>
<li>
Conduct or forward surveys, contests, pyramid schemes, or chain letters.
</li>
<li>
Impersonate another person or allow any other person or entity to use your identification to post or 
view comments.
</li>
<li>
Post the same note repeatedly (referred to as 'spamming'). Spamming is strictly prohibited.
</li>
<li>
Download any file posted by another User that a User knows, or reasonably should know, cannot be legally 
distributed through the Service.
</li>
<li>
Restrict or inhibit any other User from using and enjoying the Public Areas.
</li>
<li>
Imply or state that any statements you make are endorsed by Radserving.com, without the prior written consent 
of Radserving.com.
</li>
<li>
Use a robot, spider, manual and/or automatic processes or devices to data-mine, data- crawl, scrape or 
index Radserving.com in any manner.
</li>
<li>
Hack or interfere with Zimmber.com, its servers or any connected networks.
</li>
<li>
Adapt, alter, license, sublicense or translate Zimmber.com for your own personal or commercial use.
</li>
<li>
Remove or alter, visually or otherwise, any copyrights, trademarks or proprietary marks and rights owned 
by Radserving.com.
</li>
<li>
Upload content that is offensive and/or harmful, including, but not limited to, content that advocates, 
endorses, condones or promotes racism, bigotry, hatred or physical harm of any kind against any individual 
or group of individuals.
</li>
<li>
Upload content that provides materials or access to materials that exploit people under the age of 18 in 
an abusive, violent or sexual manner.
</li>
</ul>
All submissions made to Public Areas will be public, and radserving.com will not be responsible for the action of 
other Users with respect to any information or materials posted in Public Areas.
</p>
<h4 style="margin-top:30px">Account, Password and Security</h4>
<p>
You are the sole authorized user of your account. You are responsible for maintaining the confidentiality 
of any password and account number provided by you or Radserving.com for accessing the Service. You are solely 
and fully responsible for all activities that occur under your password or account. Radserving.com has no 
control over the use of any User's account and expressly disclaims any liability derived therefrom. 
Should you suspect that any unauthorized party may be using your password or account or you suspect any 
other breach of security, you will contact Radserving.com immediately.
</p>
<h4 style="margin-top:30px">Your Information</h4>
<p>
“Your Information” is defined as any information and materials you provide to Radserving.com or other Users 
in connection with your registration for and use of the Service, including without limitation that posted 
or transmitted for use in Public Areas. You are solely responsible for Your Information, and we act merely 
as a passive conduit for your online distribution and publication of Your Information. You hereby represent 
and warrant to Radserving.com that Your Information (a) will not be false, inaccurate, incomplete or 
misleading; (b) will not be fraudulent or involve the sale of counterfeit or stolen items; (c) will not 
infringe any third party's copyright, patent, trademark, trade secret or other proprietary right or rights 
of publicity or privacy; (d) will not violate any law, statute, ordinance, or regulation (including without 
limitation those governing export control, consumer protection, unfair competition, anti-discrimination or 
false advertising); (e) will not be defamatory, libelous, unlawfully threatening, or unlawfully harassing; 
(f) will not be obscene or contain child pornography or be harmful to minors; (g) will not contain any 
viruses, Trojan Horses, worms, time bombs, cancelbots or other computer programming routines that are 
intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data 
or personal information; and (h) will not create liability for Radserving.com or cause Radserving.com to lose 
(in whole or in part) the services of its ISPs or other partners or suppliers. 
</p>
<h4 style="margin-top:30px">Billing and Payment Policy</h4>
<p>
Radserving Customers are obligated to pay for the Services, unless specifically notified otherwise. 
</p>
<h4 style="margin-top:30px">Termination and Suspension</h4>
<p>
Without limitation, Radserving.com may terminate or suspend your right to use the Service if you breach any 
term of this Agreement or any policy of Radserving.com posted on the Service from time to time, or if 
Radserving.com otherwise finds that you have engaged in inappropriate and/or offensive behavior. If Radserving.com 
terminates or suspends your right to use the Service for any of these reasons, you will not be entitled to 
any refund of unused balance in your account. In addition to terminating or suspending your account, 
Radserving.com reserves the right to take appropriate legal action, including without limitation pursuing 
civil, criminal, and injunctive redress.
</p>
<p>
Zimmber.com may terminate or suspend your right to use the Service at anytime for any or no reason by 
providing you with written or email notice of such termination, and termination will be effective 
immediately upon delivery of such notice. Even after your right to use the Service is terminated or 
suspended, this Agreement will remain enforceable against you. 
</p>
<p>
You may terminate this Agreement at any time by ceasing all use of the Service. All sections which by 
their nature should survive the expiration or termination of this Agreement shall continue in full force 
and effect subsequent to and notwithstanding the expiration or termination of this Agreement. 
</p>
<h4 style="margin-top:30px">Intellectual Property Rights</h4>
<p>
All text, graphics, editorial content, data, formatting, graphs, designs, HTML, look and feel, photographs, 
music, sounds, images, software, videos, designs, typefaces and other content (collectively “Proprietary 
Material”) that users see or read on the Service is owned by Radserving.com or are used by permission. 
Proprietary Material is protected in all forms, media and technologies now known or hereinafter developed. 
Radserving.com owns all Proprietary Material as well as the coordination, selection, arrangement and 
enhancement of such Proprietary Materials as a Collective Work under the Intellectual Property Rights Act, 
as amended. The Proprietary Material protected by the domestic and international laws of copyright, 
patents, and other proprietary rights and laws. Users may not copy, download, use, redesign, reconfigure, 
or retransmit anything from the Service without Radserving.com's express prior written consent. Any use of 
such Proprietary Material, other than as permitted therein, is expressly prohibited without the prior 
permission of Radserving.com and/or the relevant right holder. 
</p>
<p>
The service marks and trademarks of Radserving.com, including without limitation Radserving.com and the 
Radserving.com logo are service marks owned by Radserving.com. Any other trademarks, service marks, logos and/or 
trade names appearing on the Service are the property of their respective owners. You may not copy or use 
any of these marks, logos or trade names without the express prior written consent of the owner. 
</p>
<h4 style="margin-top:30px">Modifications to the Service</h4>
<p>
Radserving.com reserves the right in its sole discretion to review, improve, modify or discontinue, 
temporarily or permanently, the Service or any content or information on the Service with or without notice. 
Radserving.com will not be liable to any party for any modification or discontinuance of the Service.
</p>
<h4 style="margin-top:30px">Confidential Information</h4>
<p>
You acknowledge that Confidential Information (as hereinafter defined) is a valuable, special and unique 
asset of Radserving.com and agree that you will not disclose, transfer, use (or seek to induce others to 
disclose, transfer or use) any Confidential Information for any purpose other than disclosure to your 
authorized employees and agents who are bound to maintain the confidentiality of Confidential Information. 
You shall promptly notify Radserving.com in writing of any circumstances which may constitute unauthorized 
disclosure, transfer, or use of Confidential Information. You shall use best efforts to protect 
Confidential Information from unauthorized disclosure, transfer or use. You shall return all originals 
and any copies of any and all materials containing Confidential Information to Radserving.com upon 
termination of this Agreement for any reason whatsoever. The term “Confidential Information” shall mean 
any and all of Radserving.com's trade secrets, confidential and proprietary information and all other 
information and data of Radserving.com that is not generally known to the public or other third parties who 
could derive value, economic or otherwise, from its use or disclosure. Confidential Information shall be 
deemed to include technical data, know-how, research, product plans, products, services, customers, 
markets, software, developments, inventions, processes, formulas, technology, designs, drawings, 
engineering, hardware configuration information, marketing, finances or other business information 
disclosed directly or indirectly in writing, orally or by drawings or observation. 
</p>
<h4 style="margin-top:30px">Disclaimer of Warranties</h4>
<p>
Radserving.com provides six (6) days warranty of services with limited liability of rework only. Whether the 
work falls in the scope of warranty is at the sole discretion of Zimmber.com based on the outcome of 
inspection of Service provided. 
</p>
<p>
NEITHER RADSERVING.COM NOR ITS AFFILIATES OR LICENSORS WARRANT THAT THE SERVICE IS FREE FROM VIRUSES, WORMS, 
TROJAN HORSES, OR OTHER HARMFUL COMPONENTS.
RADSERVING.COM AND ITS AFFILIATES AND LICENSORS CANNOT AND DO NOT GUARANTEE THAT ANY PERSONAL INFORMATION 
SUPPLIED BY YOU WILL NOT BE MISAPPROPRIATED, INTERCEPTED, DELETED, DESTROYED OR USED BY OTHERS. 
</p>
<h4 style="margin-top:30px">No Liability</h4>
<p>
YOU AGREE NOT TO HOLD RADSERVING.COM, ITS AFFILIATES, ITS LICENSORS, OR ANY OF SUCH PARTIES, AGENTS, 
EMPLOYEES, OFFICERS, DIRECTORS, CORPORATE PARTNERS, OR PARTICIPANTS LIABLE FOR ANY DAMAGE, SUITS, CLAIMS, 
AND/OR CONTROVERSIES (COLLECTIVELY, “LIABILITIES”) THAT HAVE ARISEN OR MAY ARISE, WHETHER KNOWN OR 
UNKNOWN, RELATING TO YOUR USE OF OR INABILITY TO USE THE SERVICE, INCLUDING WITHOUT LIMITATION ANY 
LIABILITIES ARISING IN CONNECTION WITH THE CONDUCT, ACT OR OMISSION OF ANY USER (INCLUDING WITHOUT 
LIMITATION STALKING, HARASSMENTTHAT IS SEXUAL OR OTHERWISE, ACTS OF PHYSICAL VIOLENCE, AND DESTRUCTION 
OF PERSONAL PROPERTY), ANY DISPUTE WITH ANY USER, ANY INSTRUCTION, ADVICE, ACT, OR SERVICE PROVIDED BY 
RADSERVING.COM OR ITS AFFILIATES OR LICENSORS AND ANY DESTRUCTION OF YOUR INFORMATION. 
</p>
<p>
UNDER NO CIRCUMSTANCES WILL RADSERVING.COM ITS AFFILIATES, ITS LICENSORS, OR ANY OF SUCH PARTIES' AGENTS, 
EMPLOYEES, OFFICERS, DIRECTORS, CORPORATE PARTNERS, OR PARTICIPANTS BE LIABLE FOR ANY DIRECT, INDIRECT, 
INCIDENTAL, CONSEQUENTIAL, SPECIAL OR EXEMPLARY DAMAGES ARISING IN CONNECTION WITH YOUR USE OF OR INABILITY 
TO USE THE SERVICES, EVEN IF ADVISED OF THE POSSIBILITY OF THE SAME. 
</p>
<p>
IF, NOTWITHSTANDING THE FOREGOING EXCLUSIONS, IT IS DETERMINED THAT RADSERVING.COM OR ITS AFFILIATES, ITS 
LICENSORS, OR ANY OF SUCH PARTIES' AGENTS, EMPLOYEES, OFFICERS, DIRECTORS, CORPORATE PARTNERS, OR 
PARTICIPANTS IS LIABLE FOR DAMAGES, IN NO EVENT WILL THE AGGREGATE LIABILITY, WHETHER ARISING IN 
CONTRACT, TORT, STRICT LIABILITY OR OTHERWISE, EXCEED THE TOTAL FEES PAID BY YOU TO RADSERVING.COM DURING 
THE three (3) MONTHS PRIOR TO THE TIME SUCH CLAIM AROSE.
</p>
<h4 style="margin-top:30px">Indemnification</h4>
<p>
You hereby agree to indemnify, defend, and hold harmless RADSERVING.com, its directors, officers, employees, 
agents, licensors, attorneys, independent contractors, providers, subsidiaries, and affiliates from and 
against any and all claim, loss, expense or demand of liability, including attorneys' fees and costs 
incurred, arising from your use or inability to use the Services.
</p>
<h4 style="margin-top:30px">General Provisions</h4>
<p>
Radserving.com takes no responsibility for money paid to Radserving Service Provider without informing us.
Radserving.com strongly discourages any direct contact with our Radserving Service Provider or any call booked 
directly by the Radserving Customer with the Radserving Service Provider, as in such cases Radserving.com has no 
control on whatsoever situation or condition. 
</p>
<p>
We discourage any tips or reward to any of our Radserving Service Provider however if any Radserving Customer 
is doing so is completely based on their own will and decision. 
</p>
<p>
We also deal with spare parts but If Radservingg customer does not want it to be from our side then 
Radserving customer should buy the materials required, for carrying out the services, on their own but 
this may delay the servicing process.
</p>
<p>
Radserving Service Provider is liable for any damage by him, in occurrence of any such event the Radserving 
Customer is requested first contact Radserving customer care and then may lodge a complaint against the 
Radserving Service Provider, if the solution is resolved amicably.
</p>
<p>
Radserving.com may decline to provide any service on National Holidays, Public Holidays, Government Holidays 
without any obligation to any customer or user. 
</p>
<p>
Radserving Customers are requested to report/inform us immediately for any misconduct or unsatisfied behavior 
of any of our Radserving Service Provider so that Radserving.com can take action for improvement. However if the 
misconduct is falling in the category of breach of law then Radserving Customer may deal with that as per the 
applicable laws directly with the Radserving Service Provider and Radserving.com will not be responsible in such 
case.
</p>
<p>
Also, in case any criminal act is committed by the Radserving Service Provider while providing Services, the 
Radserving Customer, at its own will can take direct action under the laws of country and also keep us 
informed on full details of the same. Radserving.com will not be party to any such action. 
</p>
<h4 style="margin-top:30px">Changes to this Agreement and the Service</h4>
<p>
Radserving.com reserves the right, at its sole and absolute discretion, to change, modify, add to, supplement 
or delete any of the terms and conditions of this Agreement (including the Privacy Policy) at any time, effective with or without 
prior notice. If any future changes to this Agreement are unacceptable to you or cause you to no longer be 
in compliance with this Agreement, you must terminate, and immediately stop using, the Service. Your 
continued use of the Service following any revision to this Agreement constitutes your complete and 
irrevocable acceptance of any and all such changes. Radserving.com may change, modify, suspend, or discontinue 
any aspect of the Service at any time. Radserving.com may also impose limits on certain features or restrict 
your access to parts or all of the Service without notice or liability.
I HEREBY ACKNOWLEDGE THAT I HAVE READ AND UNDERSTAND THE FOREGOING TERMS OF USE AND THE RADSERVING.COM PRIVACY 
POLICY AND AGREE THAT MY USE OF THE SERVICE IS AN ACKNOWLEDGMENT OF MY AGREEMENT TO BE BOUND BY THE TERMS 
AND CONDITIONS OF THIS AGREEMENT.
</p>
<h3 style="margin-top:40px;text-align:center">Cancellation Policy</h3>
<h4 style="margin-top:30px">Electrical Repairing</h4>
<p>
To avoid cancellation charges (50% of service charge) please cancel the order 24 hours before the scheduled 
time. For details see cancellation policy.
</p>
<h4 style="margin-top:30px">Cancellation Policy :</h4>
<ul style="list-style-type:square">
<li>
Your service request may be cancelled anytime online through Radserving platform (web). 
</li>
<li>
The cancellation charges will be 50% of service charges. 
</li>
<li>
If service request is cancelled 24 hours before the scheduled service time, the cancellation charges will 
NOT be applicable. If the service is cancelled after this, customer is liable to pay cancellation charges 
50% of service charge.
</li>
<li>
Once the product (which is to be repaired) is reached at service provider's shop then if customer want to 
return his product, customer is liable to pay 70% of service charge.
</li>
<li>
Due to any unforeseen circumstances or internal reasons, Radserving reserves the right to cancel any order 
at any point of time, and will not be liable to anyone for the same.
</li>
<li>
If the service is cancelled by the service provider or Radserving then customer is NOT liable to pay 
cancellation charges.
</li>
</ul>
<h4 style="margin-top:30px">Sanitary Works</h4>
<p>
To avoid cancellation charges (50% of service charge) please cancel the order 24 hours before the scheduled 
time. For details see cancellation policy.
</p>
<h4 style="margin-top:30px">Cancellation Policy :</h4>
<ul style="list-style-type:square">
<li>
Your service request may be cancelled anytime online through Radserving platform (web). 
</li>
<li>
The cancellation charges will be 50% of service charges. 
</li>
<li>
If service request is cancelled 24 hours before the scheduled service time, the cancellation charges will 
NOT be applicable. If the service is cancelled after this, customer is liable to pay cancellation charges 
50% of service charge.
</li>
<li>
Once the service provider visited the customer at home then if customer want to cancel the service, 
customer is liable to pay 70% of service charge.
</li>
<li>
We are align with some sanitary shops for materials and spare parts but if you choose to provide spare 
parts on yor own then we are not responsible for delay in service. 
</li>
<li>
Due to any unforeseen circumstances or internal reasons, Radserving reserves the right to cancel any order 
at any point of time, and will not be liable to anyone for the same.
</li>
<li>
If the service is cancelled by the service provider or Radserving then customer is NOT liable to pay 
cancellation charges.
</li>
</ul>
<h4 style="margin-top:30px">Furniture Repairing</h4>
<p>
To avoid cancellation charges (50% of service charge) please cancel the order 24 hours before the scheduled 
time. For details see cancellation policy.
</p>
<h4 style="margin-top:30px">Cancellation Policy :</h4>
<ul style="list-style-type:square">
<li>
Your service request may be cancelled anytime online through Radserving platform (web). 
</li>
<li>
The cancellation charges will be 50% of service charges. 
</li>
<li>
If service request is cancelled 24 hours before the scheduled service time, the cancellation charges will 
NOT be applicable. If the service is cancelled after this, customer is liable to pay cancellation charges 
50% of service charge.
</li>
<li>
Once the product (which is to be repaired) is reached at service provider's shop then if customer want to 
return his product, customer is liable to pay 70% of service charge.
</li>
<li>
Due to any unforeseen circumstances or internal reasons, Radserving reserves the right to cancel any order 
at any point of time, and will not be liable to anyone for the same.
</li>
<li>
If the service is cancelled by the service provider or Radserving then customer is NOT liable to pay 
cancellation charges.
</li>
</ul>
<h4 style="margin-top:30px">Tailoring</h4>
<p>
To avoid cancellation charges (50% of service charge) please cancel the order 24 hours before the scheduled 
time. For details see cancellation policy.
</p>
<h4 style="margin-top:30px">Cancellation Policy :</h4>
<ul style="list-style-type:square">
<li>
Your service request may be cancelled anytime online through Radserving platform (web). 
</li>
<li>
The cancellation charges will be 50% of service charges. 
</li>
<li>
If service request is cancelled 24 hours before the scheduled service time, the cancellation charges will 
NOT be applicable. If the service is cancelled after this, customer is liable to pay cancellation charges 
50% of service charge.
</li>
<li>
Once the product (which is to be tailored) is reached at service provider's shop then if customer want to 
return his product, customer is liable to pay 70% of service charge.
</li>
<li>
Due to any unforeseen circumstances or internal reasons, Radserving reserves the right to cancel any order 
at any point of time, and will not be liable to anyone for the same.
</li>
<li>
If the service is cancelled by the service provider or Radserving then customer is NOT liable to pay 
cancellation charges.
</li>
</ul>
</div>
</div>

<?php
 		require_once("footer.php");
 ?>

</body>
</html>