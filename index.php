<?php
   require_once 'classes/config.php';
   $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
   if( mysqli_connect_error()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Best Services in World</title>
		<meta itemprop="name" content="Radserving.com - online local service marketplace | Electrical repairing and sanitary works and furniture repairing and tailoring"/>
		<meta itemprop="description" content="Search for best home services in your local. Get electrical repairing and sanitary services and furniture repairing and tailoring at your home #Radserving.com" />
		<meta name="description" content="Search for best home services in your local. Get electrical repairing and sanitary services and furniture repairing and tailoring at your home #Radserving.com" />
		<meta name="keywords" content="online service marketplace, online services, online home services, Radserving, Radserving.com, home service, repairing, online home service India, electrical repairing, sanitary, plumber, furniture repairing, tailor, online sanitary works, online tailor"/>

		<!-- CSS files -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap-multiselect.css">

		<!-- js files -->
		<script src="js/jquery1.11.2.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>
		<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
		<!-- <script type="text/javascript" scr="js/serviceList.js"></script>-->
		<script type="text/javascript">

		// hide for forgot pass
		$(document).ready(function() {
			$("#forgotModal").hide();

         $("#forgotPass").click(function(){
         	$("#show").hide();
         	$("#forgotModal").fadeIn('fast');
          });

         $("#forgotBack").click(function() {
         	$("#forgotModal").hide();
         	$("#show").fadeIn('fast');
         });
		});
 

		// ajax function for show service provider list
			function showfunction(city, area, product, id)
			{
			
			  if (city == "Select City" || area == "" || product == "Select Product") {
                  document.getElementById("serviceError".concat(id)).innerHTML = "please fill all the fields";
                  return;
               }

			    if (window.XMLHttpRequest){
			        // code for IE7+, Firefox, Chrome, Opera, Safari
			        xmlhttp=new XMLHttpRequest();
			    }
			    else{// code for IE6, IE5
			        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			    }
			    xmlhttp.onreadystatechange=function(){
			        if (xmlhttp.readyState==4 && xmlhttp.status==200){
			            document.getElementById("serviceProviderList".concat(id)).innerHTML=xmlhttp.responseText;
			        }
			    }
			    xmlhttp.open("GET","ajax_practice.php?city="+city+"&area="+area+"&product="+product,true);
			    xmlhttp.send();
			}

        // ajax function for login
            function ulogin()
          {

          	var email = document.getElementById("log_email").value;
          	var password = document.getElementById("log_password").value;
          	
          	if (email == "" || password == "") {
                 document.getElementById("ulogin").innerHTML = "please fill all the fields";
                 return;
              }

              if (window.XMLHttpRequest){
			         // code for IE7+, Firefox, Chrome, Opera, Safari
			         xmlhttp=new XMLHttpRequest();
			     }
			     else{
			     // code for IE6, IE5
			         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			     }
			     xmlhttp.onreadystatechange=function(){
			         if (xmlhttp.readyState==4 && xmlhttp.status==200){
			         	var result = xmlhttp.responseText.split("\n");
			         	var bool = true;
			         	var a=document.getElementById("ulogin");
			         	for (var i = result.length - 1; i >= 0; i--) {
			         		if(result[i] == "successfull")
			         		{	
			         			bool = false;
			         			location.reload();
			         		}
			         	}
			         	if(bool)
		         			a.innerHTML = "your email or password is incorrect, please try again" ;
			       }
			     }
			     xmlhttp.open("POST","user_login.php",true);
			     xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			     xmlhttp.send("email="+email+"&password="+password);
          } 


             // ajax function for registration
            function uregistration()
          {

          	var email = document.getElementById("reg_email").value;
          	var mobile = document.getElementById("reg_mobile").value;
          	var password = document.getElementById("reg_password").value;

          	if (email == "" || password == "" || mobile == "") {
                 document.getElementById("uregister").innerHTML = "please fill all the fields";
                 return;
              }
          	
              if (window.XMLHttpRequest){
			         // code for IE7+, Firefox, Chrome, Opera, Safari
			         xmlhttp=new XMLHttpRequest();
			     }
			     else{
			     // code for IE6, IE5
			         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			     }
			     xmlhttp.onreadystatechange=function(){
			         if (xmlhttp.readyState==4 && xmlhttp.status==200){
			         	var result = xmlhttp.responseText.split("\n");
			         	var bool = true;
			         	var a=document.getElementById("uregister");
			         	for (var i = result.length - 1; i >= 0; i--) {
			         		if(result[i] == "successfull")
			         		{	
			         			bool = false;
			         			location.reload();
			         		}
			         	}
			         	if(bool)
		         			a.innerHTML = "your email is already registered with us, try with another email" ;
			       }
			     }
			     xmlhttp.open("POST","user_register.php",true);
			     xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			     xmlhttp.send("email="+email+"&mobile="+mobile+"&password="+password);
          } 


        // ajax for forgot password
         function forgotpass()
         {
         	var email = document.getElementById("forgot_email").value;
          	
              if (window.XMLHttpRequest){
			         // code for IE7+, Firefox, Chrome, Opera, Safari
			         xmlhttp=new XMLHttpRequest();
			     }
			     else{
			     // code for IE6, IE5
			         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			     }
			     xmlhttp.onreadystatechange=function(){
			         if (xmlhttp.readyState==4 && xmlhttp.status==200){
			         	var a=document.getElementById("uforgot");
			         	var result = xmlhttp.responseText;
			         	a.innerHTML = result ;
			       }
			     }
			     xmlhttp.open("POST","forgot_pass.php",true);
			     xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			     xmlhttp.send("email="+email);
         }


		</script>
	</head>

	<body>
  <!-- Google analytics -->
  <?php include_once("analyticstracking.php") ?>
  
	<!-- User Drop Down Profile menu -->
		
	<?php 
		require_once("userProfile.php");
	?>


	<!-- Header Section -->
		
	<?php 
		require_once("header.php");
	?>	
	<!-- End Header Section -->

	<!-- logIn and SignUp -->
	<?php 
		require_once("signLog.php");
	?>

	<!-- End logIn and SignUp -->

	<!-- Services Section -->
	<?php
		require_once("servicePanel.php");
	?>		
	<!-- End Of Service Section -->

	<!-- canvas -->
 	<?php
 		require_once("howWorks.php");
 	?>

    <!-- what we do -->
    <div class="container text-center" style ='background-color: #444444;margin-top: 100px;height: 350px;width: 100%;padding-top:20px'>
		<h2 style="color:#eeeeee;">What We Do At radserving</h2>
		<p style="color:#bbbbbb;margin-top:30px">We are the fastest growing online service marketplace dedicated to provide home services in<br>electronics, sanitary, furniture and tailoring domain. In electronics category we are providing<br>the repairing services for generally all electronic products. In sanitary category we are providing<br>all the major plumbing services at your doorstep.</p>
		<a href="readMore.php" target="_blank" class="btn btn-warning" role="button" style="margin-top:50px;">Read More</a>
    </div>

    <!-- certification section -->

	<div class="container" style="margin-top:30px;height:400px;margin-bottom:70px">
		<div class="row">
			<div class="col-md-6 text-center">
				<p style="margin-top:170px;font-size:30px">We Understand Our Users Value And Thier Satisfaction And Safety</p>
			</div>
			<div class="col-md-6 text-center">
				<!-- <p style="font-size:25px;margin-top:70px;">Certified</p>
				<p>All our service providers are highly skilled in thier field</p>
				<p>and undergo a examination process before listing</p>
				<p style="font-size:25px;margin-top:90px;">Record</p>
				<p>We check the criminal background and verify the address</p>
				<p>of our each and every service provider and delivery boy</p> -->
				<img src="images/radsright.png" style="width:500px;height:350px;margin-top:40px">
			</div>
		</div>
	</div>

   <!-- footer section -->

   <?php
 		require_once("footer.php");
   ?>
   
   <!-- end footer  -->


	<!-- canvas circle -->
	<script>

		 var c = document.getElementById("myCanvas1");
         var ctx = c.getContext("2d");
         ctx.fillStyle = "#f0ad4e";
         ctx.beginPath();
         ctx.arc(144, 75, 65, 0, 2 * Math.PI);
         ctx.closePath();
         ctx.fill();

         ctx.font = 60 +"px serif";
         ctx.fillStyle = 'white';
         ctx.textAlign = 'center';
         ctx.fillText('1', 144, 90);


		var c = document.getElementById("myCanvas2");
        var ctx = c.getContext("2d");
        ctx.fillStyle = "#f0ad4e";
        ctx.beginPath();
        ctx.arc(144, 75, 65, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.fill();

        ctx.font = 60 +"px serif";
        ctx.fillStyle = 'white';
        ctx.textAlign = 'center';
        ctx.fillText('2', 144, 90);


		var c = document.getElementById("myCanvas3");
        var ctx = c.getContext("2d");
        ctx.fillStyle = "#f0ad4e";
        ctx.beginPath();
        ctx.arc(144, 75, 65, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.fill();

        ctx.font = 60 +"px serif";
        ctx.fillStyle = 'white';
        ctx.textAlign = 'center';
        ctx.fillText('3', 144, 90);

	</script> 

<!-- sevice panel select box -->
	<script>
        $(document).ready(function() {
        $("#area_dropdown").multiselect({
        	enableFiltering : true,
        	buttonWidth : '100%',
        	nonSelectedText : 'select an area where you want to serviced',
        	maxHeight : 300
        });

        $("#san_area_dropdown").multiselect({
        	enableFiltering : true,
        	buttonWidth : '100%',
        	nonSelectedText : 'select an area where you want to serviced',
        	maxHeight : 300
        });

        $("#fur_area_dropdown").multiselect({
        	enableFiltering : true,
        	buttonWidth : '100%',
        	nonSelectedText : 'select an area where you want to serviced',
        	maxHeight : 300
        });

        $("#tail_area_dropdown").multiselect({
        	enableFiltering : true,
        	buttonWidth : '100%',
        	nonSelectedText : 'select an area where you want to serviced',
        	maxHeight : 300
        });

        });
		</script>

<script>
$.validate({
form : '#log'
});

$.validate({
form : '#sign'
});

 $.validate({
 form : '#electronics_form, #sanitary_form, #furniture_form, #tailor_form'
 });
 </script>

</body>
</html>

<!-- login php section -->
<?php
	if(isset($_SESSION['email']))
	{
		echo " <script> $('#loghide').replaceWith($('#userprofile'));</script>";
	}
	else
	echo " <script> $('#userprofile').hide();</script>";

?>