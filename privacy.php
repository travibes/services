<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewpoint" content="width=device-width, initial-scale=1.0">
		<title>Best Services in World</title>

		<!-- CSS files -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
       
		<!-- js files -->
		<script src="js/jquery1.11.2.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

		</head>
<body>
<!-- header -->
<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 5px;background-color:#ffffff">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
	      	<a class="navbar-brand" href="index.php"><img style="width:90px;" src="images/Drawing.png"></a>
	    </div>
		
	</div>
</nav>	

<div style="margin-bottom:60px">
<h3 style="text-align:center;margin-top:20px">Radserving</h3>
<div style="width:900px;margin:auto">
<h4 style="margin-top:40px">Privacy Policy</h4>
<p>
We respect your private information hence we want you to fully understand and become aware about our 
information collection process. This policy and all its clauses are applicable to Radserving.com which is a 
online service marketplace. Please read the following clauses to understand about our information gathering and dissemination practices. It is 
understood that by using our website Radserving website, you consent to the data gathering practices used 
anywhere in the website.
</p>
<h4 style="margin-top:30px">Important Notes:</h4>
<p>
This privacy policy is subject to change at any time without notice. To make sure you are aware of any 
changes, please review this policy periodically.
</p>
<p>
By visiting this Website you agree to be bound by the terms and conditions of this Privacy Policy. If you 
do not agree please do not use or access our Website.
</p>
<p>
By mere use of the Website, you expressly consent to our use and disclosure of your personal information in 
ccordance with this Privacy Policy. This Privacy Policy is incorporated into and subject to the Terms of Use.
</p>
<h4 style="margin-top:30px">A. Collection of Personally Details and other Information</h4>
<p>
When you use our Website, we collect and store your personal information which is provided by you from time to time. Our
primary goal in doing so is to provide you a safe, efficient, smooth and customized experience. This allows us to provide
services and features that most likely meet your needs, and to customize our Website to make your experience safer and
easier. More importantly, while doing so we collect personal information from you that we consider necessary for
achieving this purpose.
</p>
<p>
In general, you can browse the Website without telling us who you are or revealing any personal information about
yourself. Once you give us your personal information, you are not anonymous to us. Where possible, we indicate which
fields are required and which fields are optional. You always have the option to not provide information by choosing not to
use a particular service or feature on the Website. We may automatically track certain information about you based upon
your behavior on our Website. We use this information to do internal research on our users' demographics, interests, and
behavior to better understand, protect and serve our users. This information is compiled and analyzed on an aggregated
basis. This information may include the URL that you just came from (whether this URL is on our Website or not), which
URL you next go to (whether this URL is on our Website or not), your computer browser information, and your IP address.
</p>
<p>
If you transact with us, we collect some additional information, such as a billing address, a credit / debit card number and
a credit / debit card expiration date and/ or other payment instrument details and tracking information from cheques or
money orders.
</p>
<p>
We collect personally identifiable information (email address, name, phone number etc.) from you when you set up a free account with us. While you can browse some sections
of our Website without being a registered member, certain activities (such as placing an service to avail any service or
services) may not require registration. We do use your contact information to send you offers based on your previous
orders and your interests.
</p>
<h4 style="margin-top:30px">B. Use of Demographic / Profile Data / Your Information</h4>
<p>
We use personal information to provide the services you request. To the extent we use your personal information to
market to you, we will provide you the ability to opt-out of such uses. We use your personal information to resolve
disputes; troubleshoot problems; help promote a safe service; collect money; measure consumer interest in our products
and services, inform you about online and offline offers, products, services, and updates; customize your experience;
detect and protect us against error, fraud and other criminal activity; enforce our terms and conditions; and as otherwise
described to you at the time of collection.
</p>
<p>
In our efforts to continually improve our product and service offerings, we collect and analyse demographic and profile
data about our user's activity on our Website.
</p>
<p>
We identify and use your IP address to help diagnose problems with our server, and to administer our Website. Your IP
address is also used to help identify you and to gather broad demographic information.
We will occasionally ask you to complete optional online surveys. These surveys may ask you for contact information and
demographic information (like zip code, age, orincome level). We use this data to tailor your experience at our Website,
providing you with content that we think you might be interested in and to display content according to your preferences.
</p>
<h4 style="margin-top:30px">C. haring of personal information</h4>
<p>
We may share personal information with our other corporate entities and affiliates to help detect and prevent identity theft,
fraud and other potentially illegal acts; correlate related or multiple accounts to prevent abuse of our services; and to
facilitate joint or co-branded services that you request where such services are provided by more than one corporate
entity. Those entities and affiliates may not market to you as a result of such sharing unless you explicitly opt-in.
We may disclose personal information if required to do so by law or in the good faith belief that such disclosure is
reasonably necessary to respond to subpoenas, court orders, or other legal process. We may disclose personal
information to law enforcement offices, third party rights owners, or others in the good faith belief that such disclosure is
reasonably necessary to: enforce our Terms or Privacy Policy; respond to claims that an advertisement, posting or other
content violates the rights of a third party; or protect the rights, property or personal safety of our users or the general
public.
</p>
<p>
We and our affiliates will share / sell some or all of your personal information with another business entity should we (or
our assets) plan to merge with, or be acquired by that business entity, or re-organization, amalgamation, restructuring of
business. Should such a transaction occur that other business entity (or the new combined entity) will be required to follow
this privacy policy with respect to your personal information.
</p>
<h4 style="margin-top:30px">D. Links to Other Sites</h4>
<p>
Our Website links to other websites that may collect personally identifiable information about you. Radserving.com is not
responsible for the privacy practices or the content of those linked websites.
</p>
<h4 style="margin-top:30px">E. Security Precautions</h4>
<p>
Our Website has stringent security measures in place to protect the loss, misuse, and alteration of the information under
our control. Whenever you change or access your account information, we offer the use of a secure server. Once your
information is in our possession we adhere to strict security guidelines, protecting it against unauthorized access.
</p>
<h4 style="margin-top:30px">F. Opt-Out</h4>
<p>
We provide all users with the opportunity to opt-out of receiving non-essential (promotional, marketing-related)
communications from us on behalf of our partners, and from us in general, after setting up an account.
If you want to remove your contact information from all Radserving.com lists then you may contact us with request.
</p>
<h4 style="margin-top:30px">G. Advertisements on Zimmber.com</h4>
<p>
We use third-party advertising companies to serve ads when you visit our Website. These companies may use information
(not including your name, address, email address, or telephone number) about your visits to this and other websites in
order to provide advertisements about goods and services of interest to you.
</p>
<h4 style="margin-top:30px">H. Your Consent</h4>
<p>
By using the Website and/ or by providing your information, you consent to the collection and use of the information you
disclose on the Website in accordance with this Privacy Policy, including but not limited to Your consent for sharing your
information as per this privacy policy.
</p>
<p>
If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what
information we collect, how we use it, and under what circumstances we disclose it.
</p>
</div>
</div>

<?php
 		require_once("footer.php");
 ?>

</body>
</html>