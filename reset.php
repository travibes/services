<?php
   require_once 'classes/config.php';
   $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
   if( mysqli_connect_error()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewpoint" content="width=device-width, initial-scale=1.0">
		<title>Best Services in World</title>

		<!-- CSS files -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
       
		<!-- js files -->
		<script src="js/jquery1.11.2.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

		</head>
<body>
<!-- header -->
<nav class="navbar navbar-default" role="navigation" style="margin-bottom:5px;background-color:#ffffff">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
	      	<a class="navbar-brand" href="index.php"><img style="width:90px;" src="images/Drawing.png"></a>
	    </div>
		
	</div>
</nav>	

<?php
if(isset($_POST['reset']))
{
	$reset_query = "SELECT id FROM customers WHERE md5(157*2+id) = '". $_GET['uId_encrypt'] . "';";
	$result_query = mysqli_query($con,$reset_query);
	$reset_row = mysqli_fetch_assoc($result_query);
	$user_id = $reset_row['id'];
     //echo $user_id;
	$update_salt = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
	$update_saltedPW = $_POST['resetpass'] . $update_salt;
	$hashedupdatePW = hash('sha256', $update_saltedPW);

	$update_query = "UPDATE customers SET salt = '$update_salt', password = '$hashedupdatePW' WHERE id = $user_id;";
	mysqli_query($con,$update_query);

?>
<!-- password changed message -->
<div style="height:450px;text-align:center">
<div style="width:500px;height:100px;margin:auto;margin-top:50px" class="alert alert-success">
<p style="padding-top:20px">Your password has been changed successfully.</p>
</div>
</div>

<?php
}
else
{
	if(time() - $_GET['ctime']<=48*60*60)
	{
?>

<!-- form section -->
<div style="height:450px;text-align:center">
<h3 style="padding-top:100px">Reset Password</h3>
<div style="width:350px;margin:auto;margin-top:10px">
 <form action="" method="post">
	<div class="form-group">
	<input type="password" class="form-control" name="resetpass" placeholder="Enter new Password">
	</div>
	<input type="submit" class="btn btn-block btn-info" name="reset" value="Reset Password">
 </form>
</div>
</div>

<?php
}
else
{

 ?>
<!-- Link expire section -->
<div style="height:450px;text-align:center">
<div style="width:500px;height:100px;margin:auto;margin-top:50px" class="alert alert-success">
<p style="padding-top:20px">This link for reset password has been expired. please go to forgot password and get your reset link again.</p>
</div>
</div>

<?php
}
}
 		require_once("footer.php");
?>

</body>
</html>