<?php
   require_once 'classes/config.php';
   $con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
   if( mysqli_connect_error()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
   session_start();
?>


<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewpoint" content="width=device-width, initial-scale=1.0">
		<title>Best Services in World</title>

		<!-- CSS files -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.min.css">
		<!-- js files -->
		<script src="js/jquery1.11.2.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="jquery-ui/jquery-ui.min.js"></script>

		<!-- script for cancel service function -->
        <script>
          function cancelservice(serviceNumber)
          {
             if (window.XMLHttpRequest){
			        // code for IE7+, Firefox, Chrome, Opera, Safari
			        xmlhttp=new XMLHttpRequest();
			    }
			    else{// code for IE6, IE5
			        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			    }
			    xmlhttp.onreadystatechange=function(){
			        if (xmlhttp.readyState==4 && xmlhttp.status==200){
			            document.getElementById("cancel_service".concat(serviceNumber)).innerHTML=xmlhttp.responseText;
			          
			      }
			    }
			    xmlhttp.open("GET","cancel_service.php?serviceNumber="+serviceNumber,true);
			    xmlhttp.send();
          }

        </script>

		</head>
<body>
<!-- header -->
<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 5px;background-color:#ffffff">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
	      	<a class="navbar-brand" href="index.php"><img style="width:90px;" src="images/Drawing.png"></a>
	    </div>
		
	</div>
</nav>	
<!-- showing myservices -->
<div style="width: 70%;margin:auto;margin-top:40px">
	<p class="text-info" style="font-size:18px">My Services</p>
</div>

<!-- outer div for showing order accordion -->
<div style="width: 70%;margin: auto;margin-top:20px;margin-bottom:170px">
	<?php
	$order_query = "SELECT serviceNumber,serviceProviderId,orderDate,productName,serviceCharge,deliveryCharge,service_status FROM orders WHERE customerId = (SELECT id FROM customers WHERE email = '" . $_SESSION['email'] . "');";
     $order_result = mysqli_query($con,$order_query);

    if(mysqli_num_rows($order_result) > 0){
    	echo "<div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>";
       while($order_row = mysqli_fetch_assoc($order_result)) {
      // for detemining company name for each serviceprovider id
       	$servicepro_query = "SELECT companyName FROM serviceProvider WHERE id = '" . $order_row['serviceProviderId'] . "';";
        $servicepro_result = mysqli_query($con,$servicepro_query);
        $servicepro_row = mysqli_fetch_assoc($servicepro_result);
        $company = $servicepro_row['companyName'];
          
    ?>
   <!-- accordion -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="head<?php echo $order_row['serviceNumber'];?>">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $order_row['serviceNumber'];?>" aria-expanded="true" aria-controls="<?php echo $order_row['serviceNumber'];?>">
          <?php  echo $order_row['serviceNumber']; ?>
        </a>
      </h4>
    </div>
    <div id="<?php echo $order_row['serviceNumber'];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="<?php echo $order_row['serviceNumber'];?>">
      <div class="panel-body">
      <div class="row">
      <div class="col-md-4">
        <p class="text-muted">Product: <?php  echo $order_row['productName'];  ?> </p>
        <p class="text-muted" style="font-size:12px">Order date: <?php  echo $order_row['orderDate'];  ?></p>
        <p class="text-muted" style="font-size:12px">Service provider: <?php  echo $company;  ?> </p>
        </div>
        <div class="col-md-4">
        <p class="text-muted">Service charge: <b>Rs <?php  echo $order_row['serviceCharge'];  ?> </b></p>
        <div id="cancel_service<?php echo $order_row['serviceNumber'];?>"></div>
        </div>
        <div class="col-md-4">
        <p class="text-muted">Delivery charge: <b>Rs <?php  echo $order_row['deliveryCharge'];  ?> </b></p>
       <?php
       if($order_row['service_status'] == "processing") 
       {
       ?>
        <input type='hidden' id="cancel<?php echo $order_row['serviceNumber'];?>" value="<?php echo $order_row['serviceNumber']; ?>">
        <button type="button" class="btn btn-default" style="margin-top:5px" onclick="cancelservice(cancel<?php echo $order_row['serviceNumber'];?>.value)">cancel service</button>
       <?php
        }
       ?>
        </div>
        </div>
      </div>
    </div>
  </div>
    
    <?php
       }
       echo "</div>";
    }
	
	?>
</div>

<?php
require_once("footer.php");
?>

</body>
</html>